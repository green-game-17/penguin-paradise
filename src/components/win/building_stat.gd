extends HBoxContainer


func set_data(id: String):
	if id == ID.MONUMENT_SITE:
		$Image.set_texture(
			load('res://assets/buildings/monument/monument_preview.png')
		)
	else:
		$Image.set_texture(
			load('res://assets/buildings/' + id + '/' + id + '_preview.png')
		)

	$Label.set_text(
		str(StatisticsHub.get_building_stat(id))
	)
