extends Particles2D


onready var rng = RandomNumberGenerator.new()


func _ready():
	rng.randomize()

	one_shot = true
	emitting = false
	# kinda whack to set it in the scene as then we'd lose the preview

	process_material = process_material.duplicate(true)

	var timer = $Timer
	timer.connect('timeout', self, 'randomize')

	var start_delay = rng.randf() * 2
	yield(get_tree().create_timer(start_delay), "timeout")

	randomize()
	timer.start()


func randomize():
	process_material.color = Color.from_hsv(
		rng.randf(),
		process_material.color.s,
		process_material.color.v,
		process_material.color.a
	)

	var viewport_size = get_viewport().size
	var safe_distance = 200 # distance from screen edge
	position = Vector2(
		safe_distance + rng.randf() * (
			viewport_size.x - safe_distance + visibility_rect.size.x),
		safe_distance + rng.randf() * (
			viewport_size.y - safe_distance + visibility_rect.size.y)
	)

	restart()
