extends Panel


var was_already_shown = false


func _ready():
	EventManager.connect('get_save_data', self, 'get_save_data')
	EventManager.connect('set_save_data', self, 'set_save_data')

	StatisticsHub.connect('building_stat_changed', self, '_on_building_stat_changed')

	$StatsContainer/StatsPanel/VBoxContainer/ButtonContainer/ResumeButton.connect(
		'pressed', self, '_on_resume_pressed'
	)
	$StatsContainer/StatsPanel/VBoxContainer/ButtonContainer/MenuButton.connect(
		'pressed', self, '_on_menu_pressed'
	)


func _on_building_stat_changed(id, count):
	if id == ID.MONUMENT and count > 0:
		__show_win()


func _on_resume_pressed():
	hide()


func _on_menu_pressed():
	StatisticsHub.reset_data()
	SceneManager.goto_scene('menu')


func __show_win():
	if was_already_shown:
		return
	
	was_already_shown = true

	$StatsContainer/StatsPanel/VBoxContainer/StatsGrid/TimeLabel.set_text('17h 42min') # TODO
	$StatsContainer/StatsPanel/VBoxContainer/StatsGrid/PenguinLabel.set_text(
		__format_number(StatisticsHub.get_total_penguin_count())
	)
	$StatsContainer/StatsPanel/VBoxContainer/StatsGrid/FlowersLabel.set_text(
		__format_number(StatisticsHub.get_sacrified_flowers())
	)
	$StatsContainer/StatsPanel/VBoxContainer/StatsGrid/TimeLabel.set_text(
		__format_time(StatisticsHub.get_playtime())
	)

	var building_stat_grid = $StatsContainer/StatsPanel/VBoxContainer/BuildingStatContainer/BuildingStatGrid
	var building_stat_children = building_stat_grid.get_children()
	for i in range(DataHub.BUILDINGS.keys().size() - 1):
		building_stat_children[i].set_data(DataHub.BUILDINGS.keys()[i])

	show()


func __format_number(count: int) -> String:
	var text = str(count)
	if count >= 1_000_000_000:
		text = 'way too much'
	elif count >= 1_000_000:
		text = '%s %03d %03d' % [
			floor(count / 1_000_000), floor((count % 1_000_000) / 1_000), count % 1_000
		]
	elif count >= 1_000:
		text = '%s %03d' % [floor(count / 1_000), count % 1_000]
	return text


func __format_time(count: int) -> String:
	if count < 1:
		count = 1
	return '%sh %smin' % [floor(count / 60), count % 60]


func get_save_data():
	EventManager.emit_signal('send_save_data', 'win_screen', {
		'was_already_shown': was_already_shown
	})


func set_save_data(key, save_data):
	if key == 'win_screen':
		was_already_shown = save_data.get('was_already_shown', false)
