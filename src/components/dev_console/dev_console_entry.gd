extends HBoxContainer

func _ready():
	pass


func set_data_ok(command: String):
	$CommandLabel.text = command
	$ResultLabel.hide()
	$ErrorLabel.hide()


func set_data_result(command: String, result: String):
	$CommandLabel.text = command
	$OkLabel.hide()
	$ResultLabel.text = result
	$ErrorLabel.hide()


func set_data_error(command: String, error: String):
	$CommandLabel.text = command
	$OkLabel.hide()
	$ResultLabel.hide()
	$ErrorLabel.text = error
