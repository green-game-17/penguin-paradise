tool
extends PopupPanel


onready var command_scroll_container = $Container/CommandScrollContainer
onready var command_container = $Container/CommandScrollContainer/ComandContainer
onready var command_edit = $Container/CommandEdit

onready var command_entry_template = load('res://components/dev_console/dev_console_entry.tscn')

onready var is_enabled = OS.has_feature('editor')


func _ready():
	if Engine.editor_hint:
		show() # the editor just really wants to not show the panel by default...
		command_container.add_child(command_entry_template.instance())
		command_container.add_child(command_entry_template.instance())
	else:
		command_edit.text = ''
		command_edit.connect('text_entered', self, '__on_text_entered')
		connect('popup_hide', self, '__fade_last_entry')


func _input(_event):
	if is_enabled && Input.is_action_just_pressed("debug_dev_console"):
		call_deferred('popup')


func __on_text_entered(raw_command: String):
	var split_command = raw_command.split(' ')
	var command = '__cmd_%s'%split_command[0]
	split_command.remove(0)

	var new_entry = command_entry_template.instance()

	var success = true
	if has_method(command):
		var result = call(command, split_command)
		if result:
			if typeof(result) == TYPE_STRING:
				new_entry.set_data_result(raw_command, result)
			elif result.has('is_error'):
				new_entry.set_data_error(raw_command, result.get('message', 'Error'))
				success = false
			else:
				new_entry.set_data_result(raw_command, result.get('message', 'ok...'))
		else:
			new_entry.set_data_ok(raw_command)
	else:
		new_entry.set_data_error(raw_command, 'Command not defined!')
		success = false

	if success:
		command_edit.text = ''

	__fade_last_entry()
	command_container.add_child(new_entry)
	command_container.move_child(new_entry, 0)


func __fade_last_entry():
	if command_container.get_child_count() > 0:
		command_container.get_child(0).modulate = Color(1, 1, 1, 0.6)


func __cmd_clear(_args):
	for child in command_container.get_children():
		child.queue_free()


func __cmd_help(_args):
	var command_names = []
	for method in get_method_list():
		if method.name.begins_with('__cmd_'):
			command_names.append(method.name.trim_prefix('__cmd_'))
	return 'available commands:\n ' + PoolStringArray(command_names).join('\n ')


# Add custom commands below:


func __cmd_material_update(args):
	if args.size() != 2:
		return { is_error=true, message='invalid arguments! expected: <material id> <change>' }

	if not args[0] in StatisticsHub.get_known_materials():
		return { is_error=true, message='invalid material id!' }
	
	StatisticsHub.update_material_stat(args[0], int(args[1]))


func __cmd_material_update_all(_args):
	for key in StatisticsHub.get_known_materials():
		StatisticsHub.update_material_stat(key, 100_000)


func __cmd_material_ids(_args):
	return PoolStringArray(StatisticsHub.get_known_materials()).join("\n")


func __cmd_penguin_living_update(args):
	if args.size() != 2:
		return { is_error=true, message='invalid arguments! expected: <penguin id> <change>' }

	if not args[0] in StatisticsHub.get_known_penguins():
		return { is_error=true, message='invalid penguin id!' }
	
	StatisticsHub.update_penguin_stat('living', args[0], int(args[1]))


func __cmd_penguin_working_update(args):
	if args.size() != 2:
		return { is_error=true, message='invalid arguments! expected: <penguin id> <change>' }

	if not args[0] in StatisticsHub.get_known_penguins():
		return { is_error=true, message='invalid penguin id!' }
	
	StatisticsHub.update_penguin_stat('working', args[0], int(args[1]))


func __cmd_penguin_ids(_args):
	return PoolStringArray(StatisticsHub.get_known_penguins()).join("\n")


func __cmd_research_points(args):
	if args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <change>' }
	
	StatisticsHub.update_research_points(int(args[0]))


func __cmd_well_boost(args):
	if args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <boost>' }
	
	StatisticsHub.update_well_boost_increase(float(args[0]))


func __cmd_building_unlock(args):
	if args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <building id>' }

	if not args[0] in StatisticsHub.get_unlockable_buildings():
		return { is_error=true, message='invalid building id!' }

	StatisticsHub.set_building_unlocked(args[0])


func __cmd_building_unlock_all(_args):
	for key in StatisticsHub.get_unlockable_buildings():
		StatisticsHub.set_building_unlocked(key)


func __cmd_super_cheat(_args):
	__cmd_building_unlock_all([])
	__cmd_material_update_all([])

	for penguin in StatisticsHub.get_known_penguins():
		__cmd_penguin_living_update([penguin, 100])
