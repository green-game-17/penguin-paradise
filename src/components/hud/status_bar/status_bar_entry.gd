extends Control

onready var icon = $Icon
onready var amount = $PanelContainer/Details/Amount
onready var grow_indicator = $PanelContainer/Details/GrowIndicator

var indicator_down = load('res://assets/animations/indicator_down/indicator_down.tres')
var indicator_up = load('res://assets/animations/indicator_up2/indicator_up2.tres')
var indicator_basic = load('res://assets/images/indicator_basic.png')
var current_value = 0
var previous_values = [0, 0, 0]


func _ready():
	pass


func init_data(path: String):
	icon.set_texture(load('res://assets/' + path))
	amount.set_text(str(0))


func set_count(count: int):
	var text = str(count)
	if count >= 1_000_000_000:
		text = 'enough'
	elif count >= 1_000_000:
		text = '%s.%03dM' % [
			floor(count / 1_000_000), floor((count % 1_000_000) / 1_000)
		]
	elif count >= 1_000:
		text = '%s %03d' % [floor(count / 1_000), count % 1_000]
	amount.set_text(text)
	current_value = count


func _on_GrowTimer_timeout():
	var change = 0
	change += current_value - previous_values[0]
	change += current_value - previous_values[1]
	change += current_value - previous_values[2]
	if abs(change) <= 5:
		grow_indicator.set_texture(indicator_basic)
	elif change > 0:
		grow_indicator.set_texture(indicator_up)
	else:
		grow_indicator.set_texture(indicator_down)
	previous_values.append(current_value)
	previous_values.remove(0)
