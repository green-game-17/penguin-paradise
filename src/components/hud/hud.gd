extends CanvasLayer


onready var dialogue_views = {
	ID.BUILDING_PICKER: $BuildingPicker,
	ID.GATHERING_HUB: $GatheringHub,
	ID.LAB: $Lab,
	ID.LIVING_SPACE: $LivingSpace,
	ID.BLACKSMITH: $Blacksmith,
	ID.FACTORY: $Factory,
	ID.MINING_HUB: $MiningHub,
	ID.ICE_MACHINE: $IceMachine,
	ID.MONUMENT_SITE: $Monument,
	ID.STORY_VIEWER: $StoryViewer,
}

onready var panel_buttons = {
	ID.WELL: $PanelsContainer/PanelButtonsContainer/WellButton
}
onready var panel_button_textures = {
	ID.WELL: {
		'opened': load("res://assets/ui/view_well.png"),
		'closed': load("res://assets/ui/view_well_closed.png")
	}
}
onready var panel_views = {
	ID.WELL: $PanelsContainer/WellViewer
}

onready var pause_menu_view = $PauseMenu
onready var delete_mode_label = $Label


var is_dialogue_open = false
var mouse_on_gui = false
var pause_menu_open = false
var deactivate_remove_button = false
var game_paused = false


func _ready():
	EventManager.connect('game_paused', self, '_pause_game')
	EventManager.connect('building_select', self, 'hide_picker')
	EventManager.connect('open_dialogue', self, 'open_dialogue')
	EventManager.connect('pause_menu_open', self, 'set_pause_menu_open')
	
	EventManager.connect('building_placement_aborted', self, 'deactivate_button')
	StatisticsHub.connect('building_stat_changed', self, 'deactivate_button')

	$Buttons.connect('mouse_entered', self, '_on_mouse_on_gui_entered')
	$Buttons.connect('mouse_exited', self, '_on_mouse_on_gui_exited')
	$Buttons/BuildingsButton.connect('pressed', self, '_on_BuildingsButton_pressed')
	$Buttons/HintButton.connect('pressed', self, '_on_HintButton_pressed')
	$Buttons/DeleteButton.connect('pressed', self, '_on_DeleteButton_pressed')

	for id in dialogue_views:
		dialogue_views[id].connect('visibility_changed', self, '_on_dialogue_changed', [id])

	for id in panel_buttons:
		var view = panel_buttons[id]
		view.connect('pressed', self, '_on_panel_button_pressed', [id])
		view.connect('mouse_entered', self, '_on_mouse_on_gui_entered')
		view.connect('mouse_exited', self, '_on_mouse_on_gui_exited')
	
	for view in panel_views.values():
		view.connect('mouse_entered', self, '_on_mouse_on_gui_entered')
		view.connect('mouse_exited', self, '_on_mouse_on_gui_exited')


func _input(_event):
	if Input.is_action_just_pressed('pause_menu'):
		if pause_menu_view.is_visible():
			pause_menu_view.hide()
			EventManager.emit_signal('pause_menu_open', false)
			EventManager.emit_signal('game_paused', false)
		else:
			pause_menu_view.show()
			EventManager.emit_signal('pause_menu_open', true)
			EventManager.emit_signal('game_paused', true)

	if not pause_menu_open:
		if Input.is_action_just_pressed('building_selection'):
			if dialogue_views[ID.BUILDING_PICKER].is_visible():
				dialogue_views[ID.BUILDING_PICKER].hide()
			elif not is_dialogue_open:
				dialogue_views[ID.BUILDING_PICKER].show()
				if delete_mode_label.is_visible():
					_on_DeleteButton_pressed()
		elif Input.is_action_just_pressed('close_window'):
			for view in dialogue_views.values():
				if view.is_visible():
					view.hide()


func hide_picker(_name):
	dialogue_views[ID.BUILDING_PICKER].hide()
	deactivate_remove_button = true


func deactivate_button(_unneeded1=null, _unneeded2=null):
	deactivate_remove_button = false


func set_pause_menu_open(is_open: bool):
	pause_menu_open = is_open
	

func _on_BuildingsButton_pressed():
	if is_dialogue_open or game_paused:
		return 

	dialogue_views[ID.BUILDING_PICKER].show()

	if delete_mode_label.is_visible():
		_on_DeleteButton_pressed()


func _on_HintButton_pressed():#
	if is_dialogue_open:
		return

	dialogue_views[ID.STORY_VIEWER].show_last_hint_again()

	if delete_mode_label.is_visible():
		_on_DeleteButton_pressed()


func _on_DeleteButton_pressed():
	if is_dialogue_open or game_paused:
		return

	if not deactivate_remove_button:
		if delete_mode_label.is_visible():
			delete_mode_label.hide()
		else:
			delete_mode_label.show()
		EventManager.emit_signal('toggle_remove_building')

	
func unlock_well_view():
	panel_buttons[ID.WELL].show()
	_on_panel_button_pressed(ID.WELL)


func open_dialogue(building_object):
	if not is_dialogue_open:
		var id = building_object.building_type
		match building_object.building_type:
			ID.TENT, ID.HOUSE, ID.IGLOO, ID.CHARGING_STATION:
				id = ID.LIVING_SPACE
				dialogue_views[id].set_building(building_object)
			ID.GATHERING_HUB, ID.LAB, ID.BLACKSMITH, ID.FACTORY, ID.MINING_HUB, ID.ICE_MACHINE, ID.MONUMENT_SITE:
				dialogue_views[id].set_building(building_object)
		
		if dialogue_views.has(id):
			dialogue_views[id].show()


func _on_dialogue_changed(changed_dialogue_id):
	var changed_dialogue_view = dialogue_views[changed_dialogue_id]

	is_dialogue_open = false
	for view in dialogue_views.values():
		if view.is_visible():
			is_dialogue_open = true
			break
	
	if changed_dialogue_id == 'story_viewer':
		EventManager.emit_signal('story_viewer_visible', changed_dialogue_view.is_visible())

	EventManager.emit_signal('dialogue_open', is_dialogue_open)


func _on_panel_button_pressed(id):
	if panel_views[id].is_visible():
		panel_views[id].hide()
		panel_buttons[id].set_normal_texture(panel_button_textures[id]['closed'])
	else:
		panel_views[id].show()
		panel_buttons[id].set_normal_texture(panel_button_textures[id]['opened'])


func _on_mouse_on_gui_entered():
	mouse_on_gui = true
	EventManager.emit_signal('mouse_on_gui', true)


func _on_mouse_on_gui_exited():
	mouse_on_gui = false
	EventManager.emit_signal('mouse_on_gui', false)


func _pause_game(is_paused: bool):
	game_paused = is_paused
	if is_paused:
		$Buttons/PauseButton.set_text('Resume Game')
	else:
		$Buttons/PauseButton.set_text('Pause Game')


func _on_PauseButton_pressed():
	EventManager.emit_signal('game_paused', !game_paused)
