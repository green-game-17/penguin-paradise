tool
extends CanvasItem


export var key = 'E'
export var description = 'Do stuff'
export var show_plus = false


func _ready():
	$DescriptionLabel.set_text(description)
	$KeyPanel/KeyLabel.set_text(key)
	
	if show_plus:
		$PlusLabel.show()
