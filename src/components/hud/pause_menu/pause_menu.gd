extends PanelContainer


# specifies how many keys the finished savestate needs to have
const NEEDED_KEYS = 8

var savestate = {}


func _ready():
	EventManager.connect('send_save_data', self, 'add_data_to_savestate')


func _on_ResumeButton_pressed():
	hide()
	EventManager.emit_signal('pause_menu_open', false)
	EventManager.emit_signal('game_paused', false)


func _on_OptionsButton_pressed():
	$Buttons.hide()
	$Options.show()


func _on_MenuButton_pressed():
	StatisticsHub.reset_data()
	SceneManager.goto_scene('menu')


func _on_ControlButton_pressed():
	$Buttons.hide()
	$Controls.show()


func _on_BackButton_pressed():
	$Options.hide()
	$Save.hide()
	$Controls.hide()
	$Buttons.show()


func _on_SaveButton_pressed():
	$Buttons.hide()
	$Save.show()


func _on_SaveAndQuitButton_pressed():
	var savestate_name = $Save/LineEdit.get_text()
	if savestate_name == '':
		savestate_name = 'Savestate ' + str(Options.get_saves().size() + 1)
	savestate.name = savestate_name
	
	var current_time = OS.get_datetime()
	current_time.day = '%02d' % current_time.day
	current_time.month = '%02d' % current_time.month
	current_time.hour = '%02d' % current_time.hour
	current_time.minute = '%02d' % current_time.minute
	savestate.date = '{day}.{month}.{year} - {hour}:{minute}'.format(current_time)
	
	EventManager.emit_signal('get_save_data')


func add_data_to_savestate(key, data):
	savestate[key] = data
	if savestate.keys().size() == NEEDED_KEYS:
		Options.add_save(savestate)
		StatisticsHub.reset_data()
		SceneManager.goto_scene('menu')
