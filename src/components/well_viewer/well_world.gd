extends Node


## Used Unit Postifxes:
## - px  = on-screen pixel
## - vox = voxel in the texture

## Used Descriptions:
## - slice  = one background texture slice
## - patch  = one portion of water / ore
## - well   = vertical drilling
## - tunnel = horizontal drilling


# Const based on textures:
const PX_PER_VOX = 16
const SLICE_HEIGHT_VOX = 16
const SLICE_WIDTH_VOX = 32
const WELL_OFFSET_VOX = 10
const COUNT_SLICE_TEXTURES = 10
const WELL_WIDTH_VOX = 6
const COUNT_WELL_TEXTURES = 5
const TUNNEL_HEIGHT_VOX = 4
const COUNT_TUNNEL_TEXTURES = 4
const WATER_PATCH_SIZE_VOX = 8
const COUNT_WATER_PATCH_TEXTURES = 3
const ORE_PATCH_SIZE_VOX = 5
const COUNT_ORE_PATCH_TEXTURES = 4

# placeholders for balancing:
const SLICE_STACK_VISIBLE_COUNT = 6
const SLICE_STACK_START_COUNT = SLICE_STACK_VISIBLE_COUNT * 2
const INITIAL_WELL_DEPTH_BLOCKS = 17
const INITIAL_ROCK_SOLID_SLICES_COUNT = 2
const P_ORE_DOUBLE = 0.2
const P_ORE_SINGLE = 0.4
const P_WATER = 0.3
const MAX_SLICES_WITHOUT_ORES = 1
const MAX_SLICES_WITHOUT_WATER = 3
const MOVE_SPEED_PER_PENGUIN_VOX = 0.5

# quasi-constants, defined in the data hub:
var material_data = DataHub.get_generated_materials(ID.MINING_HUB)

var STONE_PER_LAYER = material_data[0].amount
var MIN_ORE_PER_PATCH = material_data[1].min_amount
var MAX_ORE_PER_PATCH = material_data[1].max_amount
var MIN_WATER_PER_PATCH = material_data[2].min_amount
var MAX_WATER_PER_PATCH = material_data[2].max_amount


## Runtime data:
var is_active = true
var game_paused = false

var slice_textures = []
var well_textures = []
var tunnel_textures = []
var water_patch_textures = []
var ore_patch_textures = []

var multiply_move_speed = 1

var generated_slices_count = 0
var well_depth_vox = 0

var last_slice_sprite_ids = [-1, -1, -1] # avoid duplicates in three consecutive slices
var slice_with_water_ago = 0
var slice_with_ore_ago = 0

var slice_sprites_buffer = []
var well_sprites_buffer = []
var tunnel_sprites_buffer = []
var water_sprite_buffer = []
var ore_sprite_buffer = []

var untapped_water_patches = []
var untapped_ore_patches = []


onready var rng = RandomNumberGenerator.new()
onready var root = $ViewportContainer/Viewport
onready var camera = $ViewportContainer/Viewport/Camera
var drill_sprite


signal depth_changed(value)


func _ready():
	rng.randomize()

	StatisticsHub.connect('well_boost_increase_changed', self, '__on_well_boost_increase_changed')
	
	EventManager.connect('game_paused', self, '_pause_game')
	EventManager.connect('get_save_data', self, '__on_get_save_data')
	EventManager.connect('set_save_data', self, '__on_set_save_data')

	# pre load textures:
	for n in range(0, COUNT_SLICE_TEXTURES):
		slice_textures.append(
			load('res://assets/well/slice_%d.png'%n)
		)
	for n in range(0, COUNT_WELL_TEXTURES):
		well_textures.append(
			load('res://assets/well/shaft_%d.png'%n)
		)
	for n in range(0, COUNT_TUNNEL_TEXTURES):
		tunnel_textures.append(
			load('res://assets/well/strip_%d.png'%n)
		)
	for n in range(0, COUNT_WATER_PATCH_TEXTURES):
		water_patch_textures.append(
			load('res://assets/well/water_%d.png'%n)
		)
	for n in range(0, COUNT_ORE_PATCH_TEXTURES):
		ore_patch_textures.append(
			load('res://assets/well/ore_%d.png'%n)
		)


func __init(loaded: bool = false):
	# fill slice sprite buffer:
	for _n in range(slice_sprites_buffer.size(), SLICE_STACK_START_COUNT * 2):
		slice_sprites_buffer.append(__init_sprite())
	for _n in range(well_sprites_buffer.size(), SLICE_STACK_VISIBLE_COUNT * SLICE_HEIGHT_VOX):
		var sprite = __init_sprite()
		sprite.offset.x = (SLICE_WIDTH_VOX - WELL_WIDTH_VOX) / 2 * PX_PER_VOX
		sprite.z_index = 1
		well_sprites_buffer.append(sprite)

	# create first stretch of slices:
	if not loaded:
		__add_slice_sprite(load('res://assets/well/slice_top.png'), 0, false, false)
		for n in range(1, SLICE_STACK_START_COUNT + 1):
			__add_random_slice(n, n > INITIAL_ROCK_SOLID_SLICES_COUNT)
		generated_slices_count = SLICE_STACK_START_COUNT

		# create first stretch of the well:
		for n in range(WELL_OFFSET_VOX, WELL_OFFSET_VOX + INITIAL_WELL_DEPTH_BLOCKS + 1):
			__add_well(n)
		well_depth_vox = WELL_OFFSET_VOX + INITIAL_WELL_DEPTH_BLOCKS
		drill_sprite = __init_sprite()
		drill_sprite.set_texture(
			load('res://assets/well/drill.png')
		)
		drill_sprite.offset.x = (SLICE_WIDTH_VOX / 2 - WELL_WIDTH_VOX / 2) * PX_PER_VOX
		drill_sprite.offset.y = (well_depth_vox + 1) * PX_PER_VOX
		drill_sprite.z_index = 3
		drill_sprite.hide()


func _physics_process(delta):
	if not is_active or game_paused:
		return

	var working_penguins = StatisticsHub.get_penguin_stat('working', ID.CHINSTRAP_PENGUIN)
	var move_speed_vox = MOVE_SPEED_PER_PENGUIN_VOX * working_penguins * multiply_move_speed * delta
	if move_speed_vox <= 0:
		return

	drill_sprite.show()
	
	camera.offset.y += move_speed_vox * PX_PER_VOX
	drill_sprite.offset.y += move_speed_vox * PX_PER_VOX

	while camera.offset.y >= slice_sprites_buffer[0].offset.y + SLICE_HEIGHT_VOX * PX_PER_VOX:
		generated_slices_count += 1
		__add_random_slice(generated_slices_count)

	while camera.offset.y / PX_PER_VOX >= well_depth_vox - INITIAL_WELL_DEPTH_BLOCKS - WELL_OFFSET_VOX:
		well_depth_vox += 1
		__add_well(well_depth_vox)
		__proceed_all_tunnels(well_depth_vox)
		StatisticsHub.update_material_stat(ID.STONE, STONE_PER_LAYER)

	emit_signal('depth_changed', INITIAL_WELL_DEPTH_BLOCKS + camera.offset.y / PX_PER_VOX)


func __on_well_boost_increase_changed(count: int):
	multiply_move_speed = 1 + count


func __add_well(n: int):
	var sprite = __get_buffered_sprite(well_sprites_buffer, 1, true)
	
	sprite.offset.y = n * PX_PER_VOX

	sprite.set_texture(
		well_textures[rng.randi_range(0, COUNT_WELL_TEXTURES - 1)]
	)
	sprite.flip_h = rand5050()


func __proceed_all_tunnels(depth: int):
	var yoinked_ores_count = __proceed_tunnels_for_one_resource(depth, untapped_ore_patches, ORE_PATCH_SIZE_VOX)
	if yoinked_ores_count > 0:
		StatisticsHub.update_material_stat(ID.MINERALS,
			yoinked_ores_count * rng.randi_range(MIN_ORE_PER_PATCH, MAX_ORE_PER_PATCH)
		)
	
	var yoinked_water_count = __proceed_tunnels_for_one_resource(depth, untapped_water_patches, WATER_PATCH_SIZE_VOX)
	if yoinked_water_count > 0:
		StatisticsHub.update_material_stat(ID.WATER,
			yoinked_water_count * rng.randi_range(MIN_WATER_PER_PATCH, MAX_WATER_PER_PATCH)
		)


func __proceed_tunnels_for_one_resource(depth: int, untapped_patches, patch_size: int) -> int:
	var slice_block_center_x = SLICE_WIDTH_VOX / 2

	var indicies_to_delete = []
	for i in range(untapped_patches.size()):
		var patch = untapped_patches[i]
		var x = patch[0]
		var y = patch[1]
		var is_left = patch[2]

		if y > depth - TUNNEL_HEIGHT_VOX:
			continue

		var sprite = __get_buffered_sprite(tunnel_sprites_buffer, patch_size)
		sprite.set_texture(
			tunnel_textures[rng.randi_range(0, COUNT_TUNNEL_TEXTURES - 1)]
		)
		sprite.z_index = 2

		var target_x = x + patch_size / 2
		var tunnel_x
		if is_left:
			tunnel_x = slice_block_center_x - 3 - (depth - y - TUNNEL_HEIGHT_VOX)
			target_x += 1
			if tunnel_x <= target_x:
				indicies_to_delete.append(i)
		else: # is_right:
			tunnel_x = slice_block_center_x + 2 + (depth - y - TUNNEL_HEIGHT_VOX)
			target_x -= 1
			if tunnel_x >= target_x:
				indicies_to_delete.append(i)
			
		sprite.offset.y = y * PX_PER_VOX
		sprite.offset.x = tunnel_x * PX_PER_VOX

	indicies_to_delete.sort()
	indicies_to_delete.invert()

	for di in indicies_to_delete:
		untapped_patches.remove(di)

	return indicies_to_delete.size()


func __add_random_slice(n: int, do_add_resources: bool = true):
	var id = null
	while id == null || last_slice_sprite_ids.has(id):
		id = rng.randi_range(0, COUNT_SLICE_TEXTURES - 1)
	last_slice_sprite_ids.pop_front()
	last_slice_sprite_ids.append(id)

	var flip_v = rng.randf() >= 0.5
	var flip_h = rng.randf() >= 0.75

	__add_slice_sprite(slice_textures[id], n, flip_v, flip_h)
	
	if not do_add_resources:
		return
	
	var is_left = rand5050()

	if slice_with_water_ago >= MAX_SLICES_WITHOUT_WATER:
		__add_random_water(n, is_left)

	elif slice_with_ore_ago >= MAX_SLICES_WITHOUT_ORES:
		__add_random_ore(n, is_left)

	elif rng.randf() < P_ORE_DOUBLE:
		__add_random_ore(n, is_left)
		__add_random_ore(n, not is_left)

	elif rng.randf() < P_ORE_SINGLE:
		__add_random_ore(n, is_left)

	elif rng.randf() < P_WATER:
		__add_random_water(n, is_left)

	else:
		slice_with_ore_ago += 1
		slice_with_water_ago += 1


func __add_random_water(n: int, is_left: bool):
	var sprite = __get_buffered_sprite(water_sprite_buffer, WATER_PATCH_SIZE_VOX)

	var x = __get_safe_x(is_left, WATER_PATCH_SIZE_VOX)
	sprite.offset.x = x * PX_PER_VOX
	sprite.set_z_index(1)
	
	var y_before = n * SLICE_HEIGHT_VOX
	var y_offset = rng.randi_range(0, SLICE_HEIGHT_VOX - WATER_PATCH_SIZE_VOX)
	var y = y_before + y_offset
	sprite.offset.y = y * PX_PER_VOX
	
	sprite.set_texture(
		water_patch_textures[rng.randi_range(0, COUNT_WATER_PATCH_TEXTURES - 1)]
	)
	sprite.flip_h = rand5050()

	untapped_water_patches.append([x, y, is_left])

	slice_with_water_ago = 0
	slice_with_ore_ago += 1


func __add_random_ore(n: int, is_left: bool):
	var sprite = __get_buffered_sprite(ore_sprite_buffer, ORE_PATCH_SIZE_VOX)

	var x = __get_safe_x(is_left, ORE_PATCH_SIZE_VOX)
	sprite.offset.x = x * PX_PER_VOX
	sprite.set_z_index(1)
	
	var y_before = n * SLICE_HEIGHT_VOX
	var y_offset = rng.randi_range(0, SLICE_HEIGHT_VOX - ORE_PATCH_SIZE_VOX)
	var y = y_before + y_offset
	sprite.offset.y = y * PX_PER_VOX
	
	sprite.set_texture(
		ore_patch_textures[rng.randi_range(0, COUNT_ORE_PATCH_TEXTURES - 1)]
	)
	sprite.flip_h = rand5050()
	sprite.flip_v = rand5050()

	untapped_ore_patches.append([x, y, is_left])

	slice_with_ore_ago = 0
	slice_with_water_ago += 1


func __get_safe_x(is_left: bool, size: int) -> int:
	if is_left:
		return rng.randi_range(
			0,
			SLICE_WIDTH_VOX / 2 - WELL_WIDTH_VOX / 2 - size
		)
	else:
		return rng.randi_range(
			SLICE_WIDTH_VOX / 2 + WELL_WIDTH_VOX / 2,
			SLICE_WIDTH_VOX - size
		)


func __add_slice_sprite(texture, n: int, flip_v: bool, flip_h: bool):
	var sprite = slice_sprites_buffer.pop_front()
	slice_sprites_buffer.append(sprite)

	sprite.set_texture(texture)
	sprite.offset.y = SLICE_HEIGHT_VOX * PX_PER_VOX * n
	sprite.flip_v = flip_v
	sprite.flip_h = flip_h


func __init_sprite():
	var sprite = Sprite.new()
	sprite.offset = Vector2(0, -1000) # spawn off screen
	sprite.centered = false
	root.add_child(sprite)
	return sprite


func __get_buffered_sprite(buffer, safezone_blocks: int, is_well: bool = false):
	var sprite
	if buffer.size() > 0 && buffer[0].offset.y < camera.offset.y - (safezone_blocks * 2 * PX_PER_VOX):
		sprite = buffer.pop_front()
	else:
		sprite = __init_sprite()
		if is_well:
			sprite.offset.x = (SLICE_WIDTH_VOX - WELL_WIDTH_VOX) / 2 * PX_PER_VOX

	buffer.append(sprite)
	return sprite


func rand5050():
	return rng.randf() > 0.5


func _pause_game(is_paused: bool):
	game_paused = is_paused


func __on_set_save_data(key, data):
	if key == 'well_world':
		if data == null:
			__init()
			return
		
		for sprite_data in data.sprites:
			var sprite = __init_sprite()
			sprite.set_offset(Vector2(sprite_data.offset.x, sprite_data.offset.y))
			sprite.set_flip_v(sprite_data.flip_v)
			sprite.set_flip_h(sprite_data.flip_h)
			var string_array = sprite_data.path.split('/')
			var file_name = string_array[string_array.size() - 1]
			if file_name.begins_with('slice'):
				for texture in slice_textures:
					if texture.get_path() == sprite_data.path:
						sprite.set_texture(texture)
						sprite.set_z_index(0)
						slice_sprites_buffer.append(sprite)
						break
				if sprite.get_texture() == null:
					sprite.set_texture(load(sprite_data.path))
					sprite.set_z_index(0)
					slice_sprites_buffer.append(sprite)
			elif file_name.begins_with('shaft'):
				for texture in well_textures:
					if texture.get_path() == sprite_data.path:
						sprite.set_texture(texture)
						sprite.set_z_index(1)
						well_sprites_buffer.append(sprite)
						break
			elif file_name.begins_with('strip'):
				for texture in tunnel_textures:
					if texture.get_path() == sprite_data.path:
						sprite.set_texture(texture)
						sprite.set_z_index(2)
						tunnel_sprites_buffer.append(sprite)
						break
			elif file_name.begins_with('water'):
				for texture in water_patch_textures:
					if texture.get_path() == sprite_data.path:
						sprite.set_texture(texture)
						sprite.set_z_index(1)
						water_sprite_buffer.append(sprite)
						break
			elif file_name.begins_with('ore'):
				for texture in ore_patch_textures:
					if texture.get_path() == sprite_data.path:
						sprite.set_texture(texture)
						sprite.set_z_index(1)
						ore_sprite_buffer.append(sprite)
						break
			elif file_name.begins_with('drill'):
				sprite.set_texture(load(sprite_data.path))
				sprite.set_z_index(3)
				drill_sprite = sprite
		for key in data.values:
			# data migrations, yay...
			if key == 'is_sideways_capable' or key == 'assigned_penguins':
				pass # depracated
			elif key == 'well_depth':
				self.well_depth_vox = data.values.well_depth
			elif key == 'current_depth':
				self.generated_slices_count = data.values.current_depth
			elif key == 'last_slice_with_stuff_ago':
				self.slice_with_water_ago = data.values.last_slice_with_stuff_ago
				self.slice_with_ore_ago = data.values.last_slice_with_stuff_ago
			else:
				self[key] = data.values[key]
		camera.set_offset(Vector2(data.camera.offset.x, data.camera.offset.y))
		__init(true)


func __on_get_save_data():
	var data = {
		'sprites': [],
		'values': {
			'is_active': is_active,
			'multiply_move_speed': multiply_move_speed,
			'generated_slices_count': generated_slices_count,
			'well_depth_vox': well_depth_vox,
			'last_slice_sprite_ids': last_slice_sprite_ids,
			'slice_with_water_ago': slice_with_water_ago,
			'slice_with_ore_ago': slice_with_ore_ago,
			'untapped_water_patches': untapped_water_patches,
			'untapped_ore_patches': untapped_ore_patches,
		},
		'camera': {
			'offset': {
				'x': camera.get_offset().x,
				'y': camera.get_offset().y,
			}
		}
	}
	for child in root.get_children():
		if child.name != 'Camera' and child.get_texture() != null:
			data.sprites.append({
				'path': child.get_texture().get_path(),
				'offset': {
					'x': child.get_offset().x,
					'y': child.get_offset().y,
				},
				'flip_v': child.is_flipped_v(),
				'flip_h': child.is_flipped_h(),
			})
	EventManager.emit_signal('send_save_data', 'well_world', data)
