extends Control


const STARTING_BAMBOO_COUNT = 7
const STARTING_WOOD_COUNT = 5
const STARTING_FISH_COUNT = 10
const STARTING_CAPE_PENGUIN_COUNT = 4

const STORY_BEATS = [
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "Our experiment worked!\nWe've reached level -1742 of reality... But we've lost the rest of our research group.\n\nLet's set up camp here and gather resources to continue our travel.",
	},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "We need food and water to support our camp. The water is covered with the puddle, but we should maybe go out and get some resources.\n\nWe need a gathering hub to collect some fish.",
	},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "You can build stuff by clicking on the build button. Then select the building you want and confirm the position by clicking.\n\nThe building can even get rotated by pressing [R].\nYou can find more controls in the pause menu, which you can open by pressing [Esc].",
	},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "This gathering hub is perfect, we can gather so many materials from here!\n\nTo get started, click on the building to select how many penguins should work at it and to focus them on fish for the start.",
		"await_building": ID.GATHERING_HUB,
	},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "Mmmm Fish! That should feed more of us - now to wait for more penguins to help us once we have more tents for them ready. We should gather bamboo for that.\n\nYou can always see how many penguins and materials we have in our village at the top of your screen.",
		"await_material": ID.FISH,
		"await_material_count": STARTING_FISH_COUNT + 1
	},
	{
		"speaker": ID.FAIRY_PENGUIN,	
		"text": "This place is great, Hmm... we should build a monument some day...\n\nAnyway - We need to get more water! The best thing is to just dig down. But we need to know which materials are needed to build the well. I could research that if I had a research lab.",
		"await_penguin": ID.FAIRY_PENGUIN,
	},
	{
		"speaker": ID.FAIRY_PENGUIN,	
		"text": "Now we can research things using all of our materials!\n\nOpen the lab to get started. First we need to convert some resources to have material to work with.",
		"await_building": ID.LAB,
	},
	{
		"speaker": ID.FAIRY_PENGUIN,	
		"text": "We now have an idea on how to mine but we don't have the skill!\n\nWe should add some nicer houses for stronger penguins, so we can dig the well. We will also need to have a place for the penguins to work.",
		"await_unlock": ID.WELL
	},
	{
		"speaker": ID.FAIRY_PENGUIN,	
		"text": "That's a great looking house - now the responsibility for a new Chinstrap resident isn't mine!",
		"await_building": ID.HOUSE
	},
	{
		"speaker": ID.CHINSTRAP_PENGUIN,	
		"text": "Let's dig yo!\n\nShow me my mining hub and I'll get cracking!",
		"await_penguin": ID.CHINSTRAP_PENGUIN,
	},
	{
		"speaker": ID.CHINSTRAP_PENGUIN,	
		"text": "We found some special rocks in the well shaft. They maybe a useful building resource, but we don't know what to do with them.",
		"await_material": ID.MINERALS,
	},
	{
		"speaker": ID.MACARONI_PENGUIN,	
		"text": "You... rock! And you also got some nice minerals there!\n\nWe could melt this down into ores for crafting if we had a blacksmith hut.",
		"await_penguin": ID.CHINSTRAP_PENGUIN,
	},
	{
		"speaker": ID.FAIRY_PENGUIN,	
		"text": "We got Kwardium - this is perfect for research! I can already feel the knowledge just by looking at the Kwardium sparkling in the light.",
		"await_material": ID.KWARDIUM,
	},
	{
		"speaker": ID.MACARONI_PENGUIN,	
		"text": "What an Ice day! We got Kalanium - this is perfect for making ice!\n\nMaybe we could research an ice making machine to make it into ice. Or even into a grand ice statue...",
		"await_material": ID.KALANIUM,
	},
	#{
	#	"speaker": ID.MACARONI_PENGUIN,	
	#	"text": "We got Tetsium - this is perfect for upgrading our tools!",
	#	"await_material": ID.TETSIUM,
	#},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "We've made so much progress! Maybe we should invite the Emperor Penguins to help us research - I've heard they live in icy Igloos...",
		"await_material": ID.ICE,
	},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "Cool! Now we can house our new neighbours! I heard they like playing with technology and batteries",
		"await_building": ID.IGLOO,
	},
	{
		"speaker": ID.EMPEROR_PENGUIN,	
		"text": "We bring knowledge of the future! Let's see what other useful materials we can extract from the well.",
		"await_penguin": ID.EMPEROR_PENGUIN,
	},
	{
		"speaker": ID.MACARONI_PENGUIN,	
		"text": "The atmosphere is positively electric!\n\nWe got Protorium - this is perfect for making batteries!",
		"await_material": ID.PROTORIUM,
	},
	{
		"speaker": ID.EMPEROR_PENGUIN,	
		"text": "We know a lot about Protorium - if only we had a Factory to process it properly.",
	},
	{
		"speaker": ID.EMPEROR_PENGUIN,	
		"text": "We're shocked! Our first production run for batteries was a great success. Do you like Robots? Our city could attract them if we had a charging station.",
		"await_material": ID.BATTERY,
	},
	{
		"speaker": ID.EMPEROR_PENGUIN,	
		"text": "Thanks for the charging station! Just a short wait and the robots will be up and running before you know it!",
		"await_building": ID.CHARGING_STATION,
	},
	{
		"speaker": ID.ROBOT_PENGUIN,	
		"text": "Bzzzzt! Thanks for our home - it's buzzing! We could work on a monument of some kind if you have a plan for one.",
		"await_penguin": ID.ROBOT_PENGUIN,
	},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "Look at that! I'm sure the Robots will help us build it if we asked them!",
		"await_building": ID.MONUMENT_SITE,
	},
	{
		"speaker": ID.ROBOT_PENGUIN,	
		"text": "Very... cool foundation you got there. Let's get buzzing to complete it and open the inter-dimensional portal.\n\nFor that we'll need a big pile of materials, you can check in the monument at any time how far we are.",
	},
	{
		"speaker": ID.CAPE_PENGUIN,	
		"text": "Look at our amazing village! What an amazing Ice Palace!",
		"await_building": ID.MONUMENT,
	},
]

var index = 0
var await_material = null
var await_material_count = null
var await_building = null
var await_penguin = null
var await_unlock = null
var is_reshowing_hint = false


onready var label = $PanelContainer/VBoxContainer/Label
onready var sprite = $PanelContainer/VBoxContainer/Label/TextureRect # TextureRect is just a Sprite thats going through and identity crisis
								  # don't @ me


var penguin_pictures = {
	ID.CAPE_PENGUIN: load('res://assets/penguins/cape/cape_penguin_icon.png'),
	ID.FAIRY_PENGUIN: load('res://assets/penguins/fairy/fairy_penguin_icon.png'),
	ID.CHINSTRAP_PENGUIN: load('res://assets/penguins/chinstrap/chinstrap_penguin_icon.png'),
	ID.MACARONI_PENGUIN: load('res://assets/penguins/macaroni/macaroni_penguin_icon.png'),
	ID.EMPEROR_PENGUIN: load('res://assets/penguins/emperor/emperor_penguin_icon.png'),
	ID.ROBOT_PENGUIN: load('res://assets/penguins/robot/robot_penguin_icon.png')
}


func _ready():
	StatisticsHub.connect('material_stat_changed', self, '__on_material_stat_changed')
	StatisticsHub.connect('penguin_stat_changed', self, '__on_penguin_stat_changed')
	StatisticsHub.connect('building_stat_changed', self, '__on_building_stat_changed')
	StatisticsHub.connect('unlock_building', self, '__on_unlock_building')
	
	EventManager.connect('start_story', self, 'start_story')
	EventManager.connect('get_save_data', self, 'get_save_data')
	
#	connect('visibility_changed', self, '__fix_height')
	
	#	$PanelContainer/VBoxContainer/HBoxContainer/NextButton.connect('pressed', self, '__on_next_btn')


func start_story(new_index):
	index = new_index
	__show_current_dialogue()


func __on_material_stat_changed(id, count):
	if await_material != null && await_material == id && count > await_material_count:
		__show_current_dialogue()


func __on_penguin_stat_changed(type, id, _count, total):
	if await_penguin != null && await_penguin == id && type == 'living' && total > 0:
		__show_current_dialogue()


func __on_building_stat_changed(id, count):
	if await_building != null && await_building == id && count > 0:
		__show_current_dialogue()


func __on_unlock_building(id):
	if await_unlock != null && await_unlock == id:
		__show_current_dialogue()


func __show_current_dialogue():
	is_reshowing_hint = false
	
	if index == 0:
		StatisticsHub.update_material_stat(ID.BAMBOO, STARTING_BAMBOO_COUNT)
		StatisticsHub.update_material_stat(ID.WOOD, STARTING_WOOD_COUNT)
		StatisticsHub.update_material_stat(ID.FISH, STARTING_FISH_COUNT)
		StatisticsHub.update_penguin_stat('living', ID.CAPE_PENGUIN, STARTING_CAPE_PENGUIN_COUNT)

	var beat = STORY_BEATS[index]

	label.text = beat.get('text', 'Ooops, no text defined...')
	sprite.set_texture(
		penguin_pictures.get(beat.get('speaker'))
	)

	if Options.get_show_story():
		__show_panel()
	else:
		__on_next_btn()  # let robot penguin press next if story dialogue is turned off


func show_last_hint_again():
	is_reshowing_hint = true

	__show_panel()


func __show_panel():
	EventManager.emit_signal('story_visible')
	show()
	__fix_height()


func __on_next_btn():
	hide()

	if not is_reshowing_hint:
		__load_next_dialogue()


# pls just look away
func __fix_height():
	if is_visible():
		var panel = $PanelContainer
		panel.set_size(Vector2(panel.get_size().x, 0))
		var diff = panel.margin_bottom + 20
		panel.margin_bottom -= diff
		panel.margin_top -= diff


func __load_next_dialogue():
	index += 1

	if index == STORY_BEATS.size():
		return

	var beat = STORY_BEATS[index]
	await_material = beat.get('await_material', null)
	await_material_count = beat.get('await_material_count', 0)
	await_building = beat.get('await_building', null)
	await_penguin = beat.get('await_penguin', null)
	await_unlock = beat.get('await_unlock', null)

	if await_material != null:
		if StatisticsHub.get_material_stat(await_material) > await_material_count:
			__show_current_dialogue()
	elif await_building != null:
		if StatisticsHub.get_building_stat(await_building) > 0:
			__show_current_dialogue()
	elif await_penguin != null:
		if StatisticsHub.get_penguin_stat('living', await_penguin) > 0:
			__show_current_dialogue()
	elif await_unlock != null:
		if StatisticsHub.is_building_unlocked(await_unlock):
			__show_current_dialogue()
	else: # we don't await anything, let's gooooooooooo
		yield(get_tree().create_timer(0.1), "timeout")
		__show_current_dialogue()


func get_save_data():
	EventManager.emit_signal('send_save_data', 'story', index)
