extends Node


# Materials:
const FISH = 'fish'
const BAMBOO = 'bamboo'
const WOOD = 'wood'
const WATER = 'water'
const STONE = 'stone'
const MINERALS = 'minerals'
const KALANIUM = 'kalanium'
const KWARDIUM = 'kwardium'
const PROTORIUM = 'protorium'
const TETSIUM = 'tetsium'
const ICE = 'ice'
const BATTERY = 'battery'

# Penguins:
const CAPE_PENGUIN = 'cape'
const FAIRY_PENGUIN = 'fairy'
const MACARONI_PENGUIN = 'macaroni'
const CHINSTRAP_PENGUIN = 'chinstrap'
const EMPEROR_PENGUIN = 'emperor'
const ROBOT_PENGUIN = 'robot'

# Buildings:
const TENT = 'tent'
const GATHERING_HUB = 'gathering_hub'
const LAB = 'lab'
const WELL = 'well'
const HOUSE = 'house'
const MINING_HUB = 'mining_hub'
const BLACKSMITH = 'blacksmith'
const FACTORY = 'factory'
const ICE_MACHINE = 'ice_machine'
const IGLOO = 'igloo'
const CHARGING_STATION = 'charging_station'
const MONUMENT_SITE = 'monument_site'
const MONUMENT = 'monument'
const LIVING_SPACE = 'living_space'
const POND = 'pond'

# internal / UI
const BUILDING_PICKER = 'building_picker'
const STORY_VIEWER = 'story_viewer'
