extends Node

const FILE_PATH_SAVE = "user://savegame.txt"

var __saved_data: Dictionary = {
	'volume': 80.0,
	'show_story': true,
	'saves': []
}
# defines if data of a savestate is loaded or not
# -1 when not data is loaded, else is index of savestate
var load_savestate = -1


func set_volume(value: float):
	__saved_data.volume = value
	__save_data()


func set_show_story(value: bool):
	__saved_data.show_story = value
	__save_data()


func add_save(new_save: Dictionary):
	__saved_data.saves.append(new_save)
	__save_data()


func set_load_savestate(index):
	load_savestate = index


func remove_save(index: int):
	__saved_data.saves.remove(index)
	__save_data()


func get_volume() -> float:
	return __saved_data.volume


func get_show_story() -> bool:
	return __saved_data.show_story


func get_saves() -> Array:
	return __saved_data.saves


func get_load_savestate() -> int:
	return load_savestate


func get_save_data() -> Dictionary:
	return __saved_data.saves[load_savestate]


func _ready():	
	__load_data()


func __save_data():
	var file = File.new()
	file.open(FILE_PATH_SAVE, File.WRITE)
	file.store_line(to_json(__saved_data))
	file.close()


func __load_data():
	var file = File.new()
	if file.file_exists(FILE_PATH_SAVE):
		file.open(FILE_PATH_SAVE, File.READ)
		var loaded_data = parse_json(file.get_line())
		file.close()
		
		# set the values to loaded values, if they exist
		# if the key doesn't exist in the loaded values, we keep the default as defined
		for key in __saved_data.keys():
			__saved_data[key] = loaded_data.get(key, __saved_data.get(key))
