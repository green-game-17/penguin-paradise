extends Node

var __material_stats = {
	ID.FISH: 0,
	ID.WOOD: 0,
	ID.BAMBOO: 0,
	ID.STONE: 0,
	ID.WATER: 0,
	ID.MINERALS: 0,
	ID.KALANIUM: 0,
	ID.TETSIUM: 0,
	ID.PROTORIUM: 0,
	ID.KWARDIUM: 0,
	ID.ICE: 0,
	ID.BATTERY: 0,
}
var __penguin_stats = {
	'living': {
		ID.CAPE_PENGUIN: 0,
		ID.FAIRY_PENGUIN: 0,
		ID.CHINSTRAP_PENGUIN: 0,
		ID.MACARONI_PENGUIN: 0,
		ID.EMPEROR_PENGUIN: 0,
		ID.ROBOT_PENGUIN: 0,
	},
	'working': {
		ID.CAPE_PENGUIN: 0,
		ID.FAIRY_PENGUIN: 0,
		ID.CHINSTRAP_PENGUIN: 0,
		ID.MACARONI_PENGUIN: 0,
		ID.EMPEROR_PENGUIN: 0,
		ID.ROBOT_PENGUIN: 0,
	}
}
var __unlocked_buildings = {
	ID.WELL: false,
	ID.HOUSE: false,
	ID.MINING_HUB: false,
	ID.BLACKSMITH: false,
	ID.FACTORY: false,
	ID.ICE_MACHINE: false,
	ID.IGLOO: false,
	ID.CHARGING_STATION: false,
	ID.MONUMENT_SITE: false,
	ID.MONUMENT: false,
}
var __building_stats = {
	ID.TENT: 0,
	ID.GATHERING_HUB: 0,
	ID.LAB: 0,
	ID.WELL: 0,
	ID.HOUSE: 0,
	ID.MINING_HUB: 0,
	ID.BLACKSMITH: 0,
	ID.FACTORY: 0,
	ID.ICE_MACHINE: 0,
	ID.IGLOO: 0,
	ID.CHARGING_STATION: 0,
	ID.MONUMENT_SITE: 0,
	ID.MONUMENT: 0,
}
var __research_points = 0
var __well_assigned_penguins = 0
var __well_boost_increase = 0.0
var __moved_out_penguins = 0
var __sacrificed_flowers = 0
var __playtime = 0


signal material_stat_changed(id, count)
signal penguin_stat_changed(type, id, count, total)
signal unlock_building(type)
signal building_stat_changed(id, count)
signal research_points_changed(count)
signal well_assigned_penguins_changed(count)
signal well_boost_increase_changed(count)


func _ready():
	EventManager.connect('get_save_data', self, 'get_save_data')


func get_material_stat(id: String) -> int:
	return __material_stats[id]


func get_known_materials():
	return __material_stats.keys()


func get_penguin_stat(type: String, id: String) -> int:
	return __penguin_stats[type][id]

	
func get_known_penguins():
	return __penguin_stats['living'].keys()


func get_total_penguin_count() -> int:
	var total = 0
	for key in __penguin_stats['living']:
		total += __penguin_stats['living'][key]
	return total + __moved_out_penguins


func is_building_unlocked(id: String) -> bool:
	return __unlocked_buildings[id]


func get_unlockable_buildings():
	return __unlocked_buildings.keys()


func get_building_stat(id: String) -> int:
	return __building_stats[id]


func get_research_points() -> int:
	return __research_points


func get_well_assigned_penguins() -> int:
	return __well_assigned_penguins


func get_well_boost_increase() -> float:
	return __well_boost_increase


func get_sacrified_flowers() -> int:
	return __sacrificed_flowers


func get_playtime() -> int:
	return __playtime


func update_material_stat(id: String, change: int) -> int:
	__material_stats[id] += change
	emit_signal('material_stat_changed', id, __material_stats[id])
	return __material_stats[id]


func update_penguin_stat(type: String, id: String, change: int) -> int:
	__penguin_stats[type][id] += change
	var total = 0
	for count in __penguin_stats[type].values():
		total += count
	emit_signal('penguin_stat_changed', type, id, __penguin_stats[type][id], total)
	if type == 'living' and change < 0:
		__moved_out_penguins -= change
	return __penguin_stats[type][id]
	
	
func set_building_unlocked(id: String):
	__unlocked_buildings[id] = true
	emit_signal('unlock_building', id)


func update_building_stat(id: String, change: int) -> int:
	__building_stats[id] += change
	emit_signal('building_stat_changed', id, __building_stats[id])
	return __building_stats[id]


func update_building_built(id: String) -> int:
	return update_building_stat(id, +1)


func update_building_demolished(id: String) -> int:
	return update_building_stat(id, -1)


func update_research_points(change: int) -> int:
	__research_points += change
	emit_signal('research_points_changed', __research_points)
	return __research_points


func update_well_assigned_penguins(change: int) -> int:
	__well_assigned_penguins += change
	emit_signal('well_assigned_penguins_changed', __well_assigned_penguins)
	return __well_assigned_penguins


func update_well_boost_increase(change: float) -> float:
	__well_boost_increase += change
	emit_signal('well_boost_increase_changed', __well_boost_increase)
	return __well_boost_increase


func update_sacrificed_flowers() -> int:
	__sacrificed_flowers += 1
	return __sacrificed_flowers


func update_playtime() -> int:
	__playtime += 1
	return __playtime


func reset_data():
	for key in __material_stats:
		__material_stats[key] = 0
	for type in __penguin_stats:
		for key in __penguin_stats[type]:
			__penguin_stats[type][key] = 0
	for key in __unlocked_buildings:
		__unlocked_buildings[key] = false
	for key in __building_stats:
		__building_stats[key] = 0
	__research_points = 0
	__well_assigned_penguins = 0
	__well_boost_increase = 0
	__moved_out_penguins = 0
	__sacrificed_flowers = 0
	__playtime = 0


func get_save_data():
	EventManager.emit_signal('send_save_data', 'stats', {
		'material_stats': __material_stats.duplicate(),
		'penguin_stats': {
			'living': __penguin_stats.living.duplicate(),
			'working': __penguin_stats.working.duplicate(),
		},
		'unlocked_buildings': __unlocked_buildings.duplicate(),
		'building_stats': __building_stats.duplicate(),
		'research_points': __research_points,
		'well_assigned_penguins': __well_assigned_penguins,
		'well_boost_increase': __well_boost_increase,
		'moved_out_penguins': __moved_out_penguins,
		'sacrificed_flowers': __sacrificed_flowers,
		'playtime': __playtime
	})


func load_save_data(save_data):
	for key in save_data.material_stats:
		if save_data.material_stats[key] > 0:
			update_material_stat(key, save_data.material_stats[key])
	for type in save_data.penguin_stats:
		for key in save_data.penguin_stats[type]:
			if save_data.penguin_stats[type][key] > 0:
				update_penguin_stat(type, key, save_data.penguin_stats[type][key])
	for type in save_data.unlocked_buildings:
		if save_data.unlocked_buildings[type]:
			if type in [ID.WELL, ID.MONUMENT_SITE, ID.MONUMENT] and save_data.building_stats[type] > 0:
				__unlocked_buildings[type] = true
			else:
				set_building_unlocked(type)
	for type in save_data.building_stats:
		update_building_stat(type, save_data.building_stats[type])
	update_research_points(save_data.research_points)
	update_well_assigned_penguins(save_data.well_assigned_penguins)
	update_well_boost_increase(save_data.well_boost_increase)
	__moved_out_penguins = save_data.get('moved_out_penguins', 0) # fallback, value added later
	__sacrificed_flowers = save_data.get('sacrificed_flowers', 0) # same
	__playtime = save_data.get('playtime', 0) # same
