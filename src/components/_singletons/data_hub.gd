extends Node


var MATERIALS = {
	ID.FISH: {
		'name': 'Flying Fish',
		'icon': load('res://assets/materials/fish.png'),
	},
	ID.WOOD: {
		'name': 'Wood',
		'icon': load('res://assets/materials/wood.png'),
	},
	ID.BAMBOO: {
		'name': 'Bamboo',
		'icon': load('res://assets/materials/bamboo.png'),
	},
	ID.STONE: {
		'name': 'Stone',
		'icon': load('res://assets/materials/stone.png'),
	},
	ID.WATER: {
		'name': 'Water',
		'icon': load('res://assets/materials/water.png'),
	},
	ID.MINERALS: {
		'name': 'Raw Minerals',
		'icon': load('res://assets/materials/minerals.png'),
	},
	ID.KALANIUM: {
		'name': 'Kalanium',
		'icon': load('res://assets/materials/kalanium.png'),
	},
	ID.TETSIUM: {
		'name': 'Tetsium',
		'icon': load('res://assets/materials/tetsium.png'),
	},
	ID.PROTORIUM: {
		'name': 'Protorium',
		'icon': load('res://assets/materials/protorium.png'),
	},
	ID.KWARDIUM: {
		'name': 'Kwardium',
		'icon': load('res://assets/materials/kwardium.png'),
	},
	ID.ICE: {
		'name': 'Ice',
		'icon': load('res://assets/materials/ice.png'),
	},
	ID.BATTERY: {
		'name': 'Battery',
		'icon': load('res://assets/materials/battery.png'),
	},
}

const BUILDINGS = {
	ID.TENT: {
		'name': 'Tent',
		'build_costs': [
			{
				'id': ID.BAMBOO,
				'amount': 3
			}
		],
		'penguin_types_living': [
			ID.CAPE_PENGUIN,
			ID.FAIRY_PENGUIN
		],
	},
	ID.HOUSE: {
		'name': 'House',
		'build_costs': [
			{
				'id': ID.BAMBOO,
				'amount': 25
			},
			{
				'id': ID.WOOD,
				'amount': 10
			}
		],
		'penguin_types_living': [
			ID.CHINSTRAP_PENGUIN,
			ID.MACARONI_PENGUIN
		],
	},
	ID.IGLOO: {
		'name': 'Igloo',
		'build_costs': [
			{
				'id': ID.ICE,
				'amount': 10
			}
		],
		'penguin_types_living': [
			ID.EMPEROR_PENGUIN
		],
	},
	ID.CHARGING_STATION: {
		'name': 'Charging Station',
		'build_costs': [
			{
				'id': ID.TETSIUM,
				'amount': 25
			},
			{
				'id': ID.BATTERY,
				'amount': 25
			}
		],
		'penguin_types_living': [
			ID.ROBOT_PENGUIN
		],
	},
	ID.GATHERING_HUB: {
		'name': 'Gathering Hub',
		'build_costs': [
			{
				'id': ID.BAMBOO,
				'amount': 2
			},
			{
				'id': ID.WOOD,
				'amount': 4
			}
		],
		'generated_materials': [
			{
				'id': ID.FISH,
				'amount': 3
			},
			{
				'id': ID.WOOD,
				'amount': 1
			},
			{
				'id': ID.BAMBOO,
				'amount': 3
			}
		],
		'penguin_type_working': ID.CAPE_PENGUIN,
	},
	ID.LAB: {
		'name': 'Research Lab',
		'build_costs': [
			{
				'id': ID.WOOD,
				'amount': 6
			}
		],
		'penguin_type_working': ID.FAIRY_PENGUIN,
	},
	ID.WELL: {
		'name': 'Well',
		'build_costs': [
			{
				'id': ID.WOOD,
				'amount': 50
			}
		],
	},
	ID.MINING_HUB: {
		'name': 'Mining Hub',
		'build_costs': [
			{
				'id': ID.BAMBOO,
				'amount': 15
			},
			{
				'id': ID.WOOD,
				'amount': 25
			}
		],
		'generated_materials': [
			{
				'id': ID.STONE,
				'amount': 1,
			},
			{
				'id': ID.MINERALS,
				'min_amount': 5,
				'max_amount': 10,
			},
			{
				'id': ID.WATER,
				'min_amount': 3,
				'max_amount': 7,
			},
		],
		'penguin_type_working': ID.CHINSTRAP_PENGUIN,
	},
	ID.BLACKSMITH: {
		'name': 'Blacksmith',
		'build_costs': [
			{
				'id': ID.STONE,
				'amount': 30
			},
			{
				'id': ID.WOOD,
				'amount': 20
			}
		],
		'generated_materials': [
			{
				'id': ID.KALANIUM,
				'amount': 1
			},
			{
				'id': ID.PROTORIUM,
				'amount': 1
			},
			{
				'id': ID.TETSIUM,
				'amount': 1
			},
			{
				'id': ID.KWARDIUM,
				'amount': 1
			}
		],
		'required_materials': [
			{
				'id': ID.MINERALS,
				'amount': 2
			}
		],
		'penguin_type_working': ID.MACARONI_PENGUIN,
	},
	ID.ICE_MACHINE: {
		'name': 'Ice machine',
		'build_costs': [
			{
				'id': ID.STONE,
				'amount': 50
			},
			{
				'id': ID.KWARDIUM,
				'amount': 20
			}
		],
		'generated_materials': [
			{
				'id': ID.ICE,
				'amount': 1
			}
		],
		'required_materials': [
			{
				'id': ID.WATER,
				'amount': 4
			},
			{
				'id': ID.KALANIUM,
				'amount': 2
			}
		],
	},
	ID.FACTORY: {
		'name': 'Factory',
		'build_costs': [
			{
				'id': ID.TETSIUM,
				'amount': 8
			},
			{
				'id': ID.STONE,
				'amount': 15
			}
		],
		'generated_materials': [
			{
				'id': ID.BATTERY,
				'amount': 1
			}
		],
		'required_materials': [
			{
				'id': ID.PROTORIUM,
				'amount': 2
			}
		],
		'penguin_type_working': ID.EMPEROR_PENGUIN,
	},
	ID.MONUMENT_SITE: {
		'name': 'Grand Monument™',
		'build_costs': [
			{
				'id': ID.BAMBOO,
				'amount': 25
			},
			{
				'id': ID.ICE,
				'amount': 100
			}
		],
		'required_materials': [
			{
				'id': ID.STONE,
				'amount': 20
			},
			{
				'id': ID.TETSIUM,
				'amount': 3
			},
			{
				'id': ID.BAMBOO,
				'amount': 5
			},
			{
				'id': ID.ICE,
				'amount': 2
			}
		],
		'penguin_type_working': ID.ROBOT_PENGUIN,
	},
	ID.MONUMENT: {
		'name': 'Grand Monument™',
	},
}

var PENGUINS = {
	ID.CAPE_PENGUIN: {
		'name': 'Cape Penguin',
		'icon': load('res://assets/penguins/cape/cape_penguin_icon.png'),
		'consumes': {
			'id': ID.FISH,
			'amount': 1
		}
	},
	ID.FAIRY_PENGUIN: {
		'name': 'Fairy Penguin',
		'icon': load('res://assets/penguins/fairy/fairy_penguin_icon.png'),
		'consumes': {
			'id': ID.FISH,
			'amount': 2
		}
	},
	ID.CHINSTRAP_PENGUIN: {
		'name': 'Chinstrap Penguin',
		'icon': load('res://assets/penguins/chinstrap/chinstrap_penguin_icon.png'),
		'consumes': {
			'id': ID.FISH,
			'amount': 3
		}
	},
	ID.MACARONI_PENGUIN: {
		'name': 'Macaroni Penguin',
		'icon': load('res://assets/penguins/macaroni/macaroni_penguin_icon.png'),
		'consumes': {
			'id': ID.FISH,
			'amount': 4
		}
	},
	ID.EMPEROR_PENGUIN: {
		'name': 'Emperor Penguin',
		'icon': load('res://assets/penguins/emperor/emperor_penguin_icon.png'),
		'consumes': {
			'id': ID.FISH,
			'amount': 6
		}
	},
	ID.ROBOT_PENGUIN: {
		'name': 'Robot Penguin',
		'icon': load('res://assets/penguins/robot/robot_penguin_icon.png'),
		'consumes': {
			'id': ID.BATTERY,
			'amount': 2
		}
	}
}

const RESEARCHES = {
	'research1': {
		'name': 'The Well',
		'description': 'Unlocks the well. With it you can dig up some minerals.',
		'infinite': false,
		'cost': 50,
		'duration': 60,
		'unlocks': ID.WELL
	},
	'research2': {
		'name': 'Housing',
		'description': 'New species of penguins can move into a house.',
		'infinite': false,
		'cost': 200,
		'duration': 150,
		'requirements': ['research1'],
		'unlocks': ID.HOUSE
	},
	'research3': {
		'name': 'Mining Base',
		'description': 'Allows to dig the well and discover underground treasures.',
		'infinite': false,
		'cost': 400,
		'duration': 280,
		'requirements': ['research2'],
		'unlocks': ID.MINING_HUB
	},
	'research4': {
		'name': 'Blacksmith',
		'description': 'A building to process raw materials into useful stuff.',
		'infinite': false,
		'cost': 500,
		'duration': 400,
		'requirements': ['research2'],
		'unlocks': ID.BLACKSMITH
	},
	'research5': {
		'name': 'Ice machine',
		'description': "Ohh no... Let's hope it doesn't break...",
		'infinite': false,
		'cost': 750,
		'duration': 550,
		'requirements': ['research3', 'research4'],
		'unlocks': ID.ICE_MACHINE
	},
	'research6': {
		'name': 'The Igloo',
		'description': 'I guess some penguins need it cold..',
		'infinite': false,
		'cost': 1_000,
		'duration': 700,
		'requirements': ['research5'],
		'unlocks': ID.IGLOO
	},
	'research7': {
		'name': 'Factory',
		'description': 'We need to power the world!',
		'infinite': false,
		'cost': 1_500,
		'duration': 880,
		'requirements': ['research6'],
		'unlocks': ID.FACTORY
	},
	'research8': {
		'name': 'Charging Stations',
		'description': 'Robo penguin is good penguin.',
		'infinite': false,
		'cost': 2_500,
		'duration': 1_200,
		'requirements': ['research7'],
		'unlocks': ID.CHARGING_STATION
	},
	'research9': {
		'name': 'Monument',
		'description': 'Hightech solution to link to wormholes.',
		'infinite': false,
		'cost': 10_000,
		'duration': 1_500,
		'requirements': ['research8'],
		'unlocks': ID.MONUMENT_SITE
	},
	'research10': {
		'name': 'Well Speed Bonus',
		'description': 'Increases well speed.',
		'infinite': true,
		'cost': 10_000,
		'duration': 1_500,
		'requirements': ['monument', 'research9'],
		'unlocks': {
			'well_boost': 0.05
		}
	}
}


func get_penguin_types_living(building_type: String) -> Array:
	return BUILDINGS[building_type].penguin_types_living


func get_generated_materials(building_type: String) -> Array:
	return BUILDINGS[building_type].generated_materials


func get_required_materials(building_type: String) -> Array:
	return BUILDINGS[building_type].required_materials


func get_building_name(building_type: String) -> String:
	return BUILDINGS[building_type].name

	
func get_penguin_type_working(building_type: String) -> String:
	return BUILDINGS[building_type].penguin_type_working


func get_food_requirements(penguin_type: String) -> Dictionary:
	return PENGUINS[penguin_type].consumes


func get_penguin_name(penguin_type: String) -> String:
	return PENGUINS[penguin_type].name


func get_penguin_icon(penguin_type: String) -> StreamTexture:
	return PENGUINS[penguin_type].icon


func get_material_name(material_type: String) -> String:
	return MATERIALS[material_type].name


func get_material_icon(material_type: String) -> StreamTexture:
	return MATERIALS[material_type].icon


func get_all_generated_materials() -> Dictionary:
	var output = {}
	for building in BUILDINGS:
		if 'generated_materials' in BUILDINGS[building]:
			output[building] = BUILDINGS[building].generated_materials
	return output


func get_building_costs(building_type) -> Array:
	return BUILDINGS[building_type].build_costs


func has_building_req_mats(building_type: String) -> bool:
	return 'required_materials' in BUILDINGS[building_type]


func has_building_gen_mats(building_type: String) -> bool:
	return 'generated_materials' in BUILDINGS[building_type]
