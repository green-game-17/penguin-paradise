extends TextureRect

func _process(_delta):
	if rect_scale.x < 2.2:
		rect_scale.x += 0.0001
	elif rect_scale.x < 4.2:
		rect_scale.x += 0.0005
		rect_scale.y += 0.0005
