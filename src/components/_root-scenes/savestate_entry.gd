extends PanelContainer


signal saves_changed()
signal start_game()


var index = 0
var save_data


func _ready():
	pass # Replace with function body.


func set_data(new_index, new_data):
	index = new_index
	$HBoxContainer/VBoxContainer/Name.set_text(new_data.name)
	$HBoxContainer/VBoxContainer/Date.set_text('Created: ' + new_data.date)


func _on_PlayButton_pressed():
	Options.set_load_savestate(index)
	emit_signal('start_game')


func _on_DeleteButton_pressed():
	Options.remove_save(index)
	emit_signal('saves_changed')
