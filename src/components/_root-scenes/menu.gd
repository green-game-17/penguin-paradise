extends Control


const FILE_PATH_SAVE = "user://savegame.txt"

onready var music = $BackgroundMusic
onready var starting_panel = $StartingPanel

var savestate_scene = load('res://components/_root-scenes/savestate_entry.tscn')


func _ready():
	randomize()
	
	music.play_menu_music() 

	var label = $CreditContainer/GameLabel
	var names = [
		'toxs1ck',
		'Momo_Hunter',
		'LauraWilkinson',
		'LucaVazz',
	]
	names.shuffle()
	label.set_text('Game by: %s, %s, %s and %s'%names)
	
	__fill_saves()


func start_game():
	starting_panel.show()
	
	# we need to stop the music and wait for to actually be stopped
	# otherwise in the browser the music playback gets messed up and massively stutters while loading
	music.stop_menu_music()
	yield(get_tree().create_timer(1.0), "timeout")
	
	SceneManager.goto_scene('world')


func _on_StartButton_pressed():
	start_game()


func _on_OptionsButton_pressed():
	$MainMenu.hide()
	$Options.show()


func _on_BackButton_pressed():
	$Options.hide()
	$Saves.hide()
	$MainMenu.show()


func _on_ContinueButton_pressed():
	$MainMenu.hide()
	$Saves.show()


func __fill_saves():
	var saves = Options.get_saves()
	if saves.size() == 0:
		if not $MainMenu.is_visible():
			$Options.hide()
			$Saves.hide()
			$MainMenu.show()
		var continue_button = $MainMenu/VBoxContainer/ContinueButton
		continue_button.set_modulate(Color(1, 1, 1, 0.4))
		continue_button.set_disabled(true)
		continue_button.set_default_cursor_shape(CURSOR_ARROW)
	else:
		var savestate_list = $Saves/VBoxContainer/ScrollContainer/VBoxContainer
		for index in range(saves.size()):
			var savestate_node = savestate_scene.instance()
			savestate_node.set_data(index, saves[index])
			savestate_node.connect('saves_changed', self, 'refresh_saves')
			savestate_node.connect('start_game', self, 'start_game')
			savestate_list.add_child(savestate_node)


func refresh_saves():
	for child in $Saves/VBoxContainer/ScrollContainer/VBoxContainer.get_children():
		child.queue_free()
	__fill_saves()
