extends Spatial


const ZOOM_BASIS = 10
const CAMERA_SPEED = 5
const CAMERA_SPEED_MOUSE = 0.0065
const GRID_SIZE = 4
const MOUSE_POS_TEXTS = {
	'empty': '',
	'no_materials': 'Not enough materials!',
	'place_blocked': 'Place already blocked!',
}


var zoom_level = 1
var story_visible = false
var dialogue_open = false
var pause_menu_open = false
var mouse_on_gui = false
var mouse_camera = false
var mouse_moved = false
var remove_building = false
var last_mouse_pos = Vector3.ZERO
var start_pos = Vector3.ZERO
var area_selection_active = false
var area_selection_aborted = false


var building_wrapper_scene = load('res://components/buildings/building_wrapper.tscn')
var buildings = {
	ID.POND: load('res://components/buildings/pond.tscn'),
	ID.HOUSE: load('res://components/buildings/house.tscn'),
	ID.IGLOO: load('res://components/buildings/igloo.tscn'),
	ID.TENT: load('res://components/buildings/tent.tscn'),
	ID.LAB: load('res://components/buildings/lab.tscn'),
	ID.WELL: load('res://components/buildings/well.tscn'),
	ID.GATHERING_HUB: load('res://components/buildings/gathering_hub.tscn'),
	ID.MINING_HUB: load('res://components/buildings/mining_hub.tscn'),
	ID.BLACKSMITH: load('res://components/buildings/blacksmith.tscn'),
	ID.FACTORY: load('res://components/buildings/factory.tscn'),
	ID.CHARGING_STATION: load('res://components/buildings/charging_station.tscn'),
	ID.ICE_MACHINE: load('res://components/buildings/ice_machine.tscn'),
	ID.MONUMENT_SITE: load('res://components/buildings/monument_site.tscn'),
	ID.MONUMENT: load('res://components/buildings/monument.tscn'),
}
var last_pointed_building_name = 'Floor'
var selected_building_data
var detail_wrapper_scene = load('res://components/environment/detail_wrapper.tscn')
var environment_details = {
	'stone1': load('res://components/environment/stone1.tscn'),
	'stone2': load('res://components/environment/stone2.tscn'),
	'stone3': load('res://components/environment/stone3.tscn'),
	'tree1': load('res://components/environment/tree1.tscn'),
	'tree2': load('res://components/environment/tree2.tscn'),
	'tree3': load('res://components/environment/tree3.tscn'),
	'bush1': load('res://components/environment/bush1.tscn'),
	'bush2': load('res://components/environment/bush2.tscn'),
	'grass1': load('res://components/environment/grass1.tscn'),
	'grass2': load('res://components/environment/grass2.tscn'),
	'grass3': load('res://components/environment/grass3.tscn'),
	'grass4': load('res://components/environment/grass4.tscn'),
	'grass5': load('res://components/environment/grass5.tscn'),
	'grass6': load('res://components/environment/grass6.tscn'),
	'grass7': load('res://components/environment/grass7.tscn'),
	'grass8': load('res://components/environment/grass8.tscn'),
	'bamboo1': load('res://components/environment/bamboo1.tscn'),
	'bamboo2': load('res://components/environment/bamboo2.tscn'),
	'flowers1': load('res://components/environment/flowers1.tscn'),
	'flowers2': load('res://components/environment/flowers2.tscn'),
	'flowers3': load('res://components/environment/flowers3.tscn'),
}


func _ready():
	randomize()
	$BackgroundMusic.play_game_music()

	__load_data()

	EventManager.connect('building_select', self, 'add_to_cursor')
	EventManager.connect('story_viewer_visible', self, 'toggle_story_visible')
	EventManager.connect('toggle_remove_building', self, 'toggle_remove_building')
	EventManager.connect('dialogue_open', self, 'set_dialogue_open')
	EventManager.connect('mouse_on_gui', self, 'set_mouse_on_gui')
	EventManager.connect('pause_menu_open', self, 'set_pause_menu_open')
	EventManager.connect('story_visible', self, 'remove_from_cursor')
	
	EventManager.connect('get_save_data', self, 'get_save_data')


func _input(event):
	if event is InputEventMouseButton:
		if not dialogue_open and not story_visible and not mouse_on_gui and not pause_menu_open:
			if Input.is_action_just_pressed('mouse_camera_move'):
				if ($MousePos/Building.get_child_count() > 0 and Input.is_action_pressed('area_selection') and
					not selected_building_data.id in [ID.MONUMENT_SITE, ID.WELL]):
					area_selection_active = true
					start_pos = $CameraAnchor/Camera/RayCast.get_collision_point().snapped(Vector3(GRID_SIZE, 10, GRID_SIZE))
				else:
					mouse_camera = true
					start_pos = $CameraAnchor.get_translation()
			elif Input.is_action_just_pressed('building_pipette') and last_pointed_building_name != 'Floor':
				var building_node = $Buildings.get_node_or_null(last_pointed_building_name)
				if building_node and not building_node.building_type in [ID.POND, ID.WELL, ID.MONUMENT_SITE, ID.MONUMENT]:
					var building_data = DataHub.BUILDINGS[building_node.building_type]
					add_to_cursor({ 
						id = building_node.building_type,
						name = building_data.name,
						costs = building_data.build_costs
					 })

			if Input.is_action_just_pressed('zoom_in'):
				zoom_level -= 0.5
				zoom_level = clamp(zoom_level, 1, 10)
				$CameraAnchor/Camera.size = zoom_level * ZOOM_BASIS
			elif Input.is_action_just_pressed('zoom_out'):
				zoom_level += 0.5
				zoom_level = clamp(zoom_level, 1, 10)
				$CameraAnchor/Camera.size = zoom_level * ZOOM_BASIS

			if $MousePos/Building.get_child_count() > 0:
				if Input.is_action_just_pressed('building_deselect'):
					EventManager.emit_signal('building_placement_aborted', selected_building_data.id)
					remove_from_cursor()
					$MousePos/Addons/Text.hide()
					for child in $GhostBuildings.get_children():
						child.queue_free()
					if area_selection_active:
						area_selection_aborted = true
				elif Input.is_action_just_released('confirm_placement') and not mouse_moved:
					if $MousePos/Building.is_visible():
						var affordable = true
						for resource in selected_building_data.costs:
							if StatisticsHub.get_material_stat(resource.id) < resource.amount:
								affordable = false
								break
						if affordable:
							for resource in selected_building_data.costs:
								StatisticsHub.update_material_stat(resource.id, -resource.amount)
							var building = $MousePos/Building.get_child(0).duplicate()
							var building_wrapper = building_wrapper_scene.instance()
							StatisticsHub.update_building_built(selected_building_data.id)
							if selected_building_data.id == ID.WELL:
								for child in $Buildings.get_children():
									if child.building_type == ID.POND:
										child.queue_free()
										break
								$CameraAnchor/Camera/HUD.unlock_well_view()
								EventManager.emit_signal('hide_building_in_picker', selected_building_data.id)
								remove_from_cursor()
							if selected_building_data.id == ID.MONUMENT_SITE:
								EventManager.emit_signal('hide_building_in_picker', selected_building_data.id)
								remove_from_cursor()
							building_wrapper.set_data(selected_building_data.id)
							building_wrapper.set_rotation(building.get_rotation())
							building.set_rotation(Vector3.ZERO)
							building_wrapper.add_child(building)
							var pos = $MousePos.get_translation()
							building_wrapper.set_translation(pos)
							for child in $EnvironmentDetails.get_children():
								var child_translation = child.get_translation()
								var to_be_removed = false
								if selected_building_data.id == ID.MONUMENT_SITE:
									if abs(child_translation.x - pos.x) <= 6 and abs(child_translation.z - pos.z) <= 6:
										to_be_removed = true
								else:
									if abs(child_translation.x - pos.x) <= 2 and abs(child_translation.z - pos.z) <= 2:
										to_be_removed = true
								if to_be_removed:
									if child.id.begins_with('flower'):
										StatisticsHub.update_sacrificed_flowers()
									child.queue_free()
							building_wrapper.connect('removed_penguin', self, '__remove_penguin')
							$Buildings.add_child(building_wrapper)
							$Placing.play()
						else:
							$MousePos/Addons/Viewport/Control/Label.set_text(MOUSE_POS_TEXTS.no_materials)
							$MousePos/Addons/Text.show()
							if $MousePos/Addons/TextTimer.is_stopped():
								$MousePos/Addons/TextTimer.start()
			else:
				if Input.is_action_just_released('select_building') and not mouse_moved:
					var collider = $CameraAnchor/Camera/RayCast.get_collider()
					if collider != null:
						if remove_building and collider.has_method('remove_self'):
							collider.remove_self()
						elif collider.has_method('open_dialogue') and not remove_building:
							collider.open_dialogue()
		if (Input.is_action_just_released('confirm_placement') and area_selection_active and
			not area_selection_aborted and mouse_moved):
			for child in $GhostBuildings.get_children():
				var affordable = true
				for resource in selected_building_data.costs:
					if StatisticsHub.get_material_stat(resource.id) < resource.amount:
						affordable = false
						break
				if affordable:
					for resource in selected_building_data.costs:
						StatisticsHub.update_material_stat(resource.id, -resource.amount)
					var building_wrapper = building_wrapper_scene.instance()
					building_wrapper.set_data(selected_building_data.id)
					StatisticsHub.update_building_built(selected_building_data.id)
					var child_duplicate = child.duplicate()
					child_duplicate.set_translation(Vector3.ZERO)
					building_wrapper.set_rotation(child_duplicate.get_rotation())
					child_duplicate.set_rotation(Vector3.ZERO)
					building_wrapper.add_child(child_duplicate)
					var pos = child.get_translation()
					building_wrapper.set_translation(child.get_translation())
					for environment_child in $EnvironmentDetails.get_children():
						var child_translation = environment_child.get_translation()
						if abs(child_translation.x - pos.x) <= 2 and abs(child_translation.z - pos.z) <= 2:
							if environment_child.id.begins_with('flower'):
								StatisticsHub.update_sacrificed_flowers()
							environment_child.queue_free()
					building_wrapper.connect('removed_penguin', self, '__remove_penguin')
					$Buildings.add_child(building_wrapper)
				else:
					break
			for child in $GhostBuildings.get_children():
				child.queue_free()
			$Placing.play()
		if Input.is_action_just_released('mouse_camera_move'):
			if mouse_camera:
				mouse_camera = false
			elif area_selection_active:
				area_selection_active = false
	elif event is InputEventKey:
		if not dialogue_open and not story_visible and not pause_menu_open:
			if Input.is_action_just_pressed('rotate_building') and $MousePos/Building.get_child_count() > 0:
				if event.get_shift():
					$MousePos/Building.get_child(0).rotation.y -= PI / 2
				else:
					$MousePos/Building.get_child(0).rotation.y += PI / 2
				for child in $GhostBuildings.get_children():
					child.set_rotation($MousePos/Building.get_child(0).get_rotation())
			elif Input.is_action_just_pressed('debug_f3'):
				if $DEBUGCursor.is_visible():
					$DEBUGCursor.hide()
				else:
					$DEBUGCursor.show()
		if Input.is_action_just_released('area_selection') and area_selection_active:
			for child in $GhostBuildings.get_children():
				child.queue_free()
			area_selection_aborted = true
	elif event is InputEventMouseMotion:
		var half_window_size = get_viewport().get_size() / 2
		var percentages = event.get_global_position() - half_window_size	
		half_window_size.x = half_window_size.y
		percentages /= half_window_size
		percentages *= 5
		var new_pos = Vector3(percentages.x * zoom_level, -percentages.y * zoom_level, 0)
		$CameraAnchor/Camera/RayCast.set_translation(new_pos)
		if not mouse_camera and not area_selection_active:
			last_mouse_pos = new_pos
		elif mouse_camera:
			var new_camera_pos = Vector3((new_pos.x - last_mouse_pos.x) * 0.7, 0, new_pos.y - last_mouse_pos.y)
			$CameraAnchor.set_translation(
				Vector3(
					start_pos.x - (new_camera_pos.x - new_camera_pos.z),
					0,
					start_pos.z - (-new_camera_pos.z - new_camera_pos.x)
				)
			)
			if new_camera_pos.length_squared() > 0.01:
				mouse_moved = true
		elif area_selection_active and not area_selection_aborted:
			var end_pos = $CameraAnchor/Camera/RayCast.get_collision_point().snapped(Vector3(GRID_SIZE, 10, GRID_SIZE))
			var obstructed_child_pos = []
			for child in $GhostBuildings.get_children():
				var child_pos = child.get_translation()
				if start_pos.x < end_pos.x and start_pos.z < end_pos.z:
					if not (child_pos.x >= start_pos.x and child_pos.x <= end_pos.x and
						child_pos.z >= start_pos.z and child_pos.z <= end_pos.z):
						child.queue_free()
					else:
						obstructed_child_pos.append(child_pos)
				elif start_pos.x < end_pos.x and start_pos.z > end_pos.z:
					if not (child_pos.x >= start_pos.x and child_pos.x <= end_pos.x and
						child_pos.z <= start_pos.z and child_pos.z >= end_pos.z):
						child.queue_free()
					else:
						obstructed_child_pos.append(child_pos)
				elif start_pos.x > end_pos.x and start_pos.z < end_pos.z:
					if not (child_pos.x <= start_pos.x and child_pos.x >= end_pos.x and
						child_pos.z >= start_pos.z and child_pos.z <= end_pos.z):
						child.queue_free()
					else:
						obstructed_child_pos.append(child_pos)
				elif start_pos.x > end_pos.x and start_pos.z > end_pos.z:
					if not (child_pos.x <= start_pos.x and child_pos.x >= end_pos.x and
						child_pos.z <= start_pos.z and child_pos.z >= end_pos.z):
						child.queue_free()
					else:
						obstructed_child_pos.append(child_pos)
				else:
					child.queue_free()
			
			var x_range
			var z_range
			# shifting is important, because ranges don't work with negative values
			var shifting = $Floor.get_scale().x
			if start_pos.x > end_pos.x:
				x_range = range(start_pos.x + shifting, end_pos.x + shifting - GRID_SIZE, -GRID_SIZE)
			else:
				x_range = range(start_pos.x + shifting, end_pos.x + shifting + GRID_SIZE, GRID_SIZE)
			if start_pos.z > end_pos.z:
				z_range = range(start_pos.z + shifting, end_pos.z + shifting - GRID_SIZE, -GRID_SIZE)
			else:
				z_range = range(start_pos.z + shifting, end_pos.z + shifting + GRID_SIZE, GRID_SIZE)
			
			for i in x_range:
				for j in z_range:
					var building_pos = Vector3(i - shifting, 0, j - shifting)
					if not building_pos in obstructed_child_pos:
						var free_place = true
						for child in $Buildings.get_children():
							if child.building_type == ID.MONUMENT_SITE:
								if (abs(child.get_translation().x - building_pos.x) <= GRID_SIZE and
									abs(child.get_translation().z - building_pos.z) <= GRID_SIZE):
									free_place = false
									break
							elif child.get_translation() == building_pos:
								free_place = false
								break
							elif (stepify($Floor.get_scale().x, GRID_SIZE) <= abs(stepify(building_pos.x, GRID_SIZE)) or 
								stepify($Floor.get_scale().z, GRID_SIZE) <= abs(stepify(building_pos.z, GRID_SIZE))):
								free_place = false
								break
						if free_place:
							var new_ghost_building = buildings[selected_building_data.id].instance()
							new_ghost_building.set_translation(building_pos)
							new_ghost_building.set_rotation($MousePos/Building.get_child(0).get_rotation())
							$GhostBuildings.add_child(new_ghost_building)
			
			var mouse_move_distance = Vector3((new_pos.x - last_mouse_pos.x) * 0.7, 0, new_pos.y - last_mouse_pos.y)
			if mouse_move_distance.length_squared() > 0.01:
				mouse_moved = true
		__move_mouse_pos()
	if not mouse_camera and not area_selection_active and mouse_moved:
		mouse_moved = false
		area_selection_aborted = false


func _process(delta):
	if not dialogue_open and not story_visible and not pause_menu_open:
		if Input.is_action_pressed('camera_left'):
			$CameraAnchor.translation.x -= CAMERA_SPEED * zoom_level * delta
			$CameraAnchor.translation.z += CAMERA_SPEED * zoom_level * delta
			__move_mouse_pos()
		if Input.is_action_pressed('camera_right'):
			$CameraAnchor.translation.x += CAMERA_SPEED * zoom_level * delta
			$CameraAnchor.translation.z -= CAMERA_SPEED * zoom_level * delta
			__move_mouse_pos()
		if Input.is_action_pressed('camera_up'):
			$CameraAnchor.translation.x -= CAMERA_SPEED * zoom_level * delta
			$CameraAnchor.translation.z -= CAMERA_SPEED * zoom_level * delta
			__move_mouse_pos()
		if Input.is_action_pressed('camera_down'):
			$CameraAnchor.translation.x += CAMERA_SPEED * zoom_level * delta
			$CameraAnchor.translation.z += CAMERA_SPEED * zoom_level * delta
			__move_mouse_pos()


func __on_cancel():
	SceneManager.goto_scene('menu')


func set_dialogue_open(is_open):
	dialogue_open = is_open


func __remove_penguin(id):
	for building in $Buildings.get_children():
		if building.workers.type == id and building.workers.amount > 0:
			building.set_working_count(building.workers.type, building.workers.amount - 1)
			StatisticsHub.update_penguin_stat('working', id, -1)
			break


func __move_mouse_pos():
	if pause_menu_open or dialogue_open:
		return
	
	var building_name = ''
	if $MousePos/Building.get_child_count() > 0:
		building_name = selected_building_data.id
	var collision_point = $CameraAnchor/Camera/RayCast.get_collision_point()
	$DEBUGCursor.set_translation(collision_point)
	var collider = $CameraAnchor/Camera/RayCast.get_collider()
	
	if collider and collider.name != 'Floor' and collider.name != last_pointed_building_name:
		var node = get_node_or_null('Buildings/' + last_pointed_building_name)
		if node and not node.building_type in [ID.WELL, ID.POND]:
			node.set_selected(false)
		var new_node = get_node_or_null('Buildings/' + collider.name)
		if new_node and not new_node.building_type in [ID.WELL, ID.POND]:
			new_node.set_selected(true)
		last_pointed_building_name = collider.name
		if $MousePos/Building.get_child_count() == 0:
			if collider.building_type in [ID.WELL, ID.POND]:
				Input.set_default_cursor_shape(Input.CURSOR_ARROW)
			else:
				Input.set_default_cursor_shape(Input.CURSOR_POINTING_HAND)
	elif collider and collider.name == 'Floor' and last_pointed_building_name != 'Floor':
		var node = get_node_or_null('Buildings/' + last_pointed_building_name)
		if node and node.building_type != ID.WELL and node.building_type != ID.POND:
			node.set_selected(false)
		last_pointed_building_name = 'Floor'
		if $MousePos/Building.get_child_count() == 0:
			Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	
	if not collider:
		$MousePos/Building.hide()
		$MousePos/Addons/Viewport/Control/Label.set_text(MOUSE_POS_TEXTS.empty)
	elif not $MousePos/Building.is_visible():
		$MousePos/Building.show()
		if not $MousePos/Addons/TextTimer.is_stopped():
			$MousePos/Addons/Viewport/Control/Label.set_text(MOUSE_POS_TEXTS.no_materials)
	
	if (stepify($Floor.scale.x, GRID_SIZE) <= abs(stepify(collision_point.x, GRID_SIZE)) or 
		stepify($Floor.scale.z, GRID_SIZE) <= abs(stepify(collision_point.z, GRID_SIZE))):
		$MousePos/Building.hide()
		$MousePos/Addons/Viewport/Control/Label.set_text(MOUSE_POS_TEXTS.empty)
	
	if building_name == ID.WELL:
		collision_point.x = 0
		collision_point.z = 0
	
	collision_point.x = stepify(collision_point.x, GRID_SIZE)
	collision_point.y = 0
	collision_point.z = stepify(collision_point.z, GRID_SIZE)
	$MousePos.set_translation(collision_point)

	var free_place = true
	var mouse_pos = $MousePos
	for child in $Buildings.get_children():
		if building_name == ID.WELL:
			break
		elif ($MousePos/Building.get_child_count() > 0 and selected_building_data.id == ID.MONUMENT_SITE) or child.building_type == ID.MONUMENT_SITE:
			var child_trans = child.get_translation()
			var mouse_trans = mouse_pos.get_translation()
			if (child_trans.x >= mouse_trans.x - GRID_SIZE && child_trans.x <= mouse_trans.x + GRID_SIZE &&
				child_trans.z >= mouse_trans.z - GRID_SIZE && child_trans.z <= mouse_trans.z + GRID_SIZE):
				free_place = false
				break
		elif child.get_translation() == mouse_pos.get_translation():
			free_place = false
			break
	
	if not free_place:
		$MousePos/Building.hide()
		if $MousePos/Building.get_child_count() > 0:
			$MousePos/Addons/Viewport/Control/Label.set_text(MOUSE_POS_TEXTS.place_blocked)
			$MousePos/Addons/Text.show()
	elif $MousePos/Addons/TextTimer.is_stopped():
		$MousePos/Addons/Text.hide()


func add_to_cursor(building_data):
	selected_building_data = building_data
	remove_from_cursor()
	$MousePos/Building.add_child(buildings[building_data.id].instance())
	$MousePos/Building.hide()


func remove_from_cursor():
	if $MousePos/Building.get_child_count() > 0:
		$MousePos/Building.remove_child($MousePos/Building.get_child(0))


func __generate_environment_details():
	var noise = OpenSimplexNoise.new()
	noise.set_seed(randi())
	noise.set_period(24.0)
	var area = $Floor.get_scale() - Vector3(2, 0, 2)
	for i in range(area.x * 2):
		for j in range(area.z * 2):
			var too_close = false
			var noise_value = noise.get_noise_2d(i, j)
			var pos = Vector3(i - area.x, 0, j - area.z)
			for child in $Buildings.get_children():
				if abs(child.get_translation().x - pos.x) <= 2 and abs(child.get_translation().z - pos.z) <= 2:
					too_close = true
					break
			if noise_value > 0 and randi() % 4 == 0 and not too_close:
				var detail_id = environment_details.keys()[randi() % environment_details.keys().size()]
				var environment_scene = environment_details[detail_id]
				var environment_node = environment_scene.instance()
				var wrapper_node = detail_wrapper_scene.instance()
				wrapper_node.set_id(detail_id)
				wrapper_node.add_child(environment_node)
				wrapper_node.set_translation(pos)
				wrapper_node.set_rotation(Vector3(0, (PI / 2) * (randi() % 4), 0))
				$EnvironmentDetails.add_child(wrapper_node)


func toggle_story_visible(is_visible):
	story_visible = is_visible


func set_mouse_on_gui(is_on):
	mouse_on_gui = is_on


func toggle_remove_building():
	remove_building = !remove_building


func set_pause_menu_open(is_open: bool):
	pause_menu_open = is_open


func get_save_data():
	var building_objects = []
	var environment_detail_objects = []
	for child in $Buildings.get_children():
		building_objects.append(child.get_save_data())
	for child in $EnvironmentDetails.get_children():
		environment_detail_objects.append({
			'id': child.id,
			'translation': {
				'x': child.get_translation().x,
				'y': child.get_translation().y,
				'z': child.get_translation().z
			},
			'rotation': {
				'x': child.get_rotation().x,
				'y': child.get_rotation().y,
				'z': child.get_rotation().z,
			}
		})
	EventManager.emit_signal('send_save_data', 'world', {
		'buildings': building_objects,
		'environment_details': environment_detail_objects,
		'well_view_unlocked': StatisticsHub.get_building_stat(ID.WELL) == 1
	})


func __load_data():
	if Options.get_load_savestate() == -1:
		__generate_environment_details()
		EventManager.emit_signal('start_story', 0)
		EventManager.emit_signal('set_save_data', 'well_world', null)
		return
	
	var save_data = Options.get_save_data()
	EventManager.emit_signal('set_save_data', 'win_screen', save_data.get('win_screen', {}))
	StatisticsHub.load_save_data(save_data.stats)
	EventManager.emit_signal('start_story', save_data.story - 1)
	EventManager.emit_signal('set_save_data', 'lab', save_data.lab)
	EventManager.emit_signal('set_save_data', 'well_world', save_data.well_world)
	for child in $Buildings.get_children():
		child.queue_free()
	for entry in save_data.world.buildings:
		var building_node = building_wrapper_scene.instance()
		building_node.add_child(buildings[entry.type].instance())
		building_node.connect('removed_penguin', self, '__remove_penguin')
		$Buildings.add_child(building_node)
		building_node.set_save_data(entry)
	for entry in save_data.world.environment_details:
		var detail_node = detail_wrapper_scene.instance()
		detail_node.add_child(environment_details[entry.id].instance())
		detail_node.set_id(entry.id)
		detail_node.set_translation(Vector3(entry.translation.x, entry.translation.y, entry.translation.z))
		detail_node.set_rotation(Vector3(entry.rotation.x, entry.rotation.y, entry.rotation.z))
		$EnvironmentDetails.add_child(detail_node)
	if save_data.world.well_view_unlocked:
		$CameraAnchor/Camera/HUD.unlock_well_view()


func _on_TextTimer_timeout():
	$MousePos/Addons/Text.hide()


func _on_PlaytimeTimer_timeout():
	StatisticsHub.update_playtime()
