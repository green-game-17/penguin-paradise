extends Control


func _ready():
	connect('visibility_changed', self, '__refresh_values')
	set_audio_bus_volume()


func set_audio_bus_volume():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'), Options.get_volume() - 80)


func __refresh_values():
	if is_visible():
		$Volume/Value.set_text(str(Options.get_volume()))
		$Volume/Slider.set_value(Options.get_volume())
		$Story/Value.set_pressed(Options.get_show_story())


func _on_Slider_value_changed(value: float):
	$Volume/Value.set_text(str(value))
	Options.set_volume(value)
	set_audio_bus_volume()


func _on_Value_toggled(button_pressed: bool):
	Options.set_show_story(button_pressed)
