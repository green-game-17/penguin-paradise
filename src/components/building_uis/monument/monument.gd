extends Building


onready var needs_column = $VBoxContainer/CenterContainer/HBoxContainer/Needs
onready var working_amount_label = $VBoxContainer/CenterContainer/HBoxContainer/Penguins/MarginContainer/VBoxContainer/Working/Amount
onready var available_amount_label = $VBoxContainer/CenterContainer/HBoxContainer/Penguins/MarginContainer/VBoxContainer/Available/Amount
onready var progress_bar_step = $VBoxContainer/ProgressBarStep
onready var progress_bar_total = $VBoxContainer/ProgressBarTotal


func _ready():
	$VBoxContainer/Title.set_title(ID.MONUMENT_SITE)
	needs_column.init_materials(ID.MONUMENT_SITE)
	progress_bar_step.init_data('Constructing parts', 20)
	progress_bar_total.init_data('Constructing whole monument', 500)


func set_building(new_building_object):
	.set_building(new_building_object)
	__refresh_dialogue()


func set_progress(value: float):
	progress_bar_step.set_value(value)


func __refresh_dialogue():
	__set_working()
	progress_bar_total.set_value(building_object.monument_progress)


func __set_working():
	working_amount_label.set_text(str(building_object.resources_paid_for))
	available_amount_label.set_text(str(StatisticsHub.get_penguin_stat('living', ID.ROBOT_PENGUIN)))
