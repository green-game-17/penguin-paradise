extends Building


onready var penguins_column = $VBoxContainer/CenterContainer/HBoxContainer/Penguins
onready var needs_column = $VBoxContainer/CenterContainer/HBoxContainer/Needs
onready var produces_column = $VBoxContainer/CenterContainer/HBoxContainer/Produces
onready var progress_bar = $VBoxContainer/ProgressBar


func _ready():
	$VBoxContainer/Title.set_title(ID.FACTORY)
	penguins_column.init_penguin_type(ID.FACTORY)
	penguins_column.connect('planned_changed', self, 'set_working_penguin')
	needs_column.init_materials(ID.FACTORY)
	produces_column.init_materials(ID.FACTORY)
	produces_column.connect('produce_triggered', self, 'change_produces')
	progress_bar.init_data('Producing', 20)


func set_building(new_building_object):
	.set_building(new_building_object)
	__refresh_dialogue()


func change_produces(id: String, active: bool):
	emit_signal('produce_changed', id, active)
	__refresh_dialogue()	


func set_progress(value: float):
	progress_bar.set_value(value)
	

func __refresh_dialogue():
	penguins_column.set_working(building_object.resources_paid_for)
	penguins_column.set_planned(building_object.workers.amount)

	produces_column.set_materials_state(building_object.produces, building_object.resources_paid_for)

	set_progress(building_object.collect_timer)
