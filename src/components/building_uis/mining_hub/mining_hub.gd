extends Building


onready var penguins_column = $VBoxContainer/CenterContainer/HBoxContainer/Penguins
onready var produces_column = $VBoxContainer/CenterContainer/HBoxContainer/Produces
onready var center_container = $VBoxContainer/CenterContainer
onready var no_well_label = $VBoxContainer/NoWellLabel
onready var speed_amount_label = $VBoxContainer/VBoxContainer/Speed/Amount
onready var bonus_amount_label = $VBoxContainer/VBoxContainer/Bonus/Amount


func _ready():
	$VBoxContainer/Title.set_title(ID.MINING_HUB)
	penguins_column.init_penguin_type(ID.MINING_HUB, true, 'Mining')
	penguins_column.connect('planned_changed', self, 'set_working_penguin')
	produces_column.init_materials(ID.MINING_HUB)
	produces_column.connect('produce_triggered', self, 'change_produces')
	StatisticsHub.connect('well_assigned_penguins_changed', self, '_on_well_assigned_penguins_changed')
	StatisticsHub.connect('well_boost_increase_changed', self, '_on_well_boost_increase_changed')


func set_building(new_building_object):
	.set_building(new_building_object)
	__refresh_dialogue()


func set_working_penguin(new_count: int, penguin_type: String):
	if StatisticsHub.get_building_stat(ID.WELL) == 0:
		return
	
	.set_working_penguin(new_count, penguin_type)


func change_produces(id: String, active: bool):
	emit_signal('produce_changed', id, active)
	__refresh_dialogue()


func _on_well_assigned_penguins_changed(count: int):
	if count == 0:
		speed_amount_label.add_color_override('font_color', Color(1, 1, 1, 0.7))
	else:
		speed_amount_label.add_color_override('font_color', Color(0.19, 0.42, 0.42, 1))
	speed_amount_label.set_text(str(count * 0.5) + 'm/s')


func _on_well_boost_increase_changed(count: float):
	if count == 0:
		bonus_amount_label.add_color_override('font_color', Color(1, 1, 1, 0.7))
	else:
		bonus_amount_label.add_color_override('font_color', Color(0.19, 0.42, 0.42, 1))
	bonus_amount_label.set_text(str(count * 100) + '%')


func __refresh_dialogue():
	penguins_column.set_working(building_object.resources_paid_for)
	penguins_column.set_planned(building_object.workers.amount)

	if StatisticsHub.get_building_stat(ID.WELL) == 1:
		no_well_label.hide()
		center_container.show()
	else:
		no_well_label.show()
		center_container.hide()
