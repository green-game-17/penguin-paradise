extends PanelContainer


class_name Building


signal new_working_count(count)
signal produce_changed(id, active)


var building_object


func _ready():
	StatisticsHub.connect('penguin_stat_changed', self, '_on_penguin_stat_changed')
	connect('visibility_changed', self, '__hidden')

	$VBoxContainer/Title.connect('closePressed', self, 'hide')


func set_building(new_building_object: Object):
	building_object = new_building_object
	connect('new_working_count', building_object, 'set_working_count')
	connect('produce_changed', building_object, 'change_produces')
	building_object.connect('set_progress', self, 'set_progress')
	building_object.connect('new_production', self, '__refresh_dialogue')


func __hidden():
	if not is_visible():
		disconnect('new_working_count', building_object, 'set_working_count')
		disconnect('produce_changed', building_object, 'change_produces')
		building_object.disconnect('set_progress', self, 'set_progress')
		building_object.disconnect('new_production', self, '__refresh_dialogue')
		__play_sound(false)


func set_progress(value: float):
	pass


func change_produces(id: String, active: bool):
	pass


func set_working_penguin(new_count: int, penguin_type: String):
	var living_penguins = StatisticsHub.get_penguin_stat('living', penguin_type)
	var working_penguins = StatisticsHub.get_penguin_stat('working', penguin_type)
	if new_count - building_object.workers.amount > living_penguins - working_penguins:
		new_count = living_penguins - working_penguins + building_object.workers.amount
	
	StatisticsHub.update_penguin_stat('working', penguin_type, new_count - building_object.workers.amount)
	emit_signal('new_working_count', penguin_type, new_count)
	__play_sound(false)
	__refresh_dialogue()


func _on_penguin_stat_changed(type: String, id: String, count: int, total: int):
	pass


func __refresh_dialogue():
	pass


func __play_sound(on: bool):
	pass
