extends ProgressBar


onready var label = $HBoxContainer/Label
onready var value_label = $HBoxContainer/Value


func init_data(label_text: String, max_value: int):
	set_max(max_value)

	set_label(label_text)
	set_value(0)


func set_value(value: float):
	.set_value(value)

	value_label.set_text(
		str(int(value / max_value * 100)) + '%'
	)


func set_label(label_text: String, show_value_label: bool = true):
	if not label_text.ends_with('...'):
		label_text = label_text + '...'
	label.set_text(label_text)

	if show_value_label:
		value_label.show()
	else:
		value_label.hide()
