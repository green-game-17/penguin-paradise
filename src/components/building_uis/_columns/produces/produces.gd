tool
extends VBoxContainer


onready var container = $MarginContainer/VBoxContainer
onready var entry_scene = load('res://components/building_uis/_columns/produces/produces_entry.tscn')

var materials
var entry_nodes_dict = {}


signal produce_triggered(id, active)


func _ready():
	if Engine.editor_hint:
		for _i in range(4):
			var entry_node = entry_scene.instance()
			container.add_child(entry_node)


func init_materials(id: String):
	materials = DataHub.get_generated_materials(id)
	var hide_toggle = materials.size() == 1 or id == ID.MINING_HUB

	if hide_toggle:
		$Label.set_text('Produces:')

	for entry_data in materials:
		var entry_node = entry_scene.instance()
		if 'amount' in entry_data:
			entry_node.init_data(entry_data.id, entry_data.amount, true, hide_toggle)
			if not hide_toggle:
				entry_node.connect('produce_triggered', self, '__on_produce_triggered')
		else:
			entry_node.init_data_for_mining(entry_data.id, entry_data.min_amount, entry_data.max_amount)
		container.add_child(entry_node)
		entry_nodes_dict[entry_data.id] = entry_node


func set_materials_state(produced_materials, resources_paid_for):
	for mat in materials:
		var entry_node = entry_nodes_dict[mat.id]
		if mat.id in produced_materials:
			entry_node.set_toggle(true)
			entry_node.set_amount_multiplier(resources_paid_for)
		else:
			entry_node.set_toggle(false)
			entry_node.set_amount_multiplier(0)


func __on_produce_triggered(id: String, active: bool):
	emit_signal('produce_triggered', id, active)
