extends HBoxContainer


signal produce_triggered(id, active)


var mat_id
var base_amount = null
var base_min_amount = null
var base_max_amount = null

var game_paused = false


func _ready():
	EventManager.connect('game_paused', self, '_pause_game')


func init_data(id: String, amount: int, active: bool, hide_toggle: bool = false):
	mat_id = id
	base_amount = amount

	$Image.set_texture(DataHub.get_material_icon(id))
	$Name.set_text(DataHub.get_material_name(id))
	set_amount_multiplier(1)
	if hide_toggle:
		$ActiveTrigger.hide()
	else:
		set_toggle(active)


func init_data_for_mining(id: String, min_amount: int, max_amount: int):
	mat_id = id
	base_min_amount = min_amount
	base_max_amount = max_amount

	$Image.set_texture(DataHub.get_material_icon(id))
	$Name.set_text(DataHub.get_material_name(id))
	set_amount_multiplier(1)
	$ActiveTrigger.hide()

	
func set_amount_multiplier(multiplier: int):
	if base_amount != null:
		$Count.set_text(str(multiplier * base_amount))
	elif base_min_amount != null and base_max_amount != null:
		$Count.set_text('%d - %d'%[multiplier * base_min_amount, multiplier * base_max_amount])


func set_toggle(active: bool):
	$ActiveTrigger.set_pressed(active)
	if active:
		set_modulate(Color(1, 1, 1, 1))
	else:
		set_modulate(Color(1, 1, 1, 0.4))


func _on_ActiveTrigger_pressed():
	if game_paused:
		$ActiveTrigger.set_pressed(!$ActiveTrigger.is_pressed())
		return
	
	emit_signal('produce_triggered', mat_id, $ActiveTrigger.is_pressed())


func _pause_game(is_paused: bool):
	game_paused = is_paused
