tool
extends VBoxContainer


onready var container = $MarginContainer/VBoxContainer
onready var entry_scene = load('res://components/building_uis/_columns/needs/needs_entry.tscn')

var materials
var entry_nodes_dict = {}


func _ready():
	if Engine.editor_hint:
		for _i in range(4):
			var entry_node = entry_scene.instance()
			container.add_child(entry_node)


func init_materials(id: String, hide_per_penguin: bool = false):
	materials = DataHub.get_required_materials(id)

	for entry_data in materials:
		var entry_node = entry_scene.instance()
		entry_node.init_data(entry_data.id, entry_data.amount)
		container.add_child(entry_node)
		entry_nodes_dict[entry_data.id] = entry_node

	if hide_per_penguin:
		$Label.set_text('Needs:')
