extends HBoxContainer


var mat_id


func init_data(id: String, amount: int):
	mat_id = id

	$Image.set_texture(DataHub.get_material_icon(id))
	$Name.set_text(DataHub.get_material_name(id))
	$Amount.set_text(str(amount))
