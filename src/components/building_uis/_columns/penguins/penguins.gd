extends VBoxContainer


const active_color = Color(1, 1, 1, 1)
const inactive_color = Color(0.4, 0.4, 0.4, 0.7)


onready var working_sprites = [
	$MarginContainer/VBoxContainer/Working/Working1,
	$MarginContainer/VBoxContainer/Working/Working2,
	$MarginContainer/VBoxContainer/Working/Working3
]
onready var planned_sprites = [
	$MarginContainer/VBoxContainer/Planned/Planned1,
	$MarginContainer/VBoxContainer/Planned/Planned2,
	$MarginContainer/VBoxContainer/Planned/Planned3
]
onready var available_label = $MarginContainer/VBoxContainer/Available/Amount

var penguin_type
var penguin_texture
var assigned_penguins = 0

var game_paused = false


signal planned_changed(new_amount)


func _ready():
	EventManager.connect('game_paused', self, '_pause_game')
	for i in range(3):
		planned_sprites[i].connect('pressed', self, '__on_planned_pressed', [i+1])


func init_penguin_type(id, no_planned: bool = false, custom_working_text = null):
	penguin_type = DataHub.get_penguin_type_working(id)
	penguin_texture = DataHub.get_penguin_icon(penguin_type)
	for i in range(3):
		working_sprites[i].set_texture(penguin_texture)
		planned_sprites[i].set_normal_texture(penguin_texture)
		
	if no_planned:
		$MarginContainer/VBoxContainer/Working.hide()
		
	if custom_working_text != null:
		$MarginContainer/VBoxContainer/Planned/Label.set_text(custom_working_text)

	StatisticsHub.connect('penguin_stat_changed', self, '__on_penguin_stat_changed')
	
				
func set_working(amount: int):
	if amount >= 1:
		working_sprites[0].set_modulate(active_color)
	else:
		working_sprites[0].set_modulate(inactive_color)
	if amount >= 2:
		working_sprites[1].set_modulate(active_color)
	else:
		working_sprites[1].set_modulate(inactive_color)
	if amount == 3:
		working_sprites[2].set_modulate(active_color)
	else:
		working_sprites[2].set_modulate(inactive_color)


func set_planned(amount: int):
	if amount >= 1:
		planned_sprites[0].set_modulate(active_color)
	else:
		planned_sprites[0].set_modulate(inactive_color)
	if amount >= 2:
		planned_sprites[1].set_modulate(active_color)
	else:
		planned_sprites[1].set_modulate(inactive_color)
	if amount == 3:
		planned_sprites[2].set_modulate(active_color)
	else:
		planned_sprites[2].set_modulate(inactive_color)

	assigned_penguins = amount


func __on_penguin_stat_changed(type: String, id: String, count: int, _total: int):
	if (id == penguin_type):
		var living = count
		var working = count
		if (type == 'living'):
			working = StatisticsHub.get_penguin_stat('working', penguin_type)
		else:
			living = StatisticsHub.get_penguin_stat('living', penguin_type)
		available_label.set_text(str(living - working))


func __on_planned_pressed(new_amount: int):
	if game_paused:
		return
	
	if (new_amount == assigned_penguins):
		new_amount -= 1

	emit_signal('planned_changed', new_amount, penguin_type)


func _pause_game(is_paused: bool):
	game_paused = is_paused
