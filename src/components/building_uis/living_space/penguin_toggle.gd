extends HBoxContainer

onready var image = $Image
onready var name_label = $Name
onready var active_trigger = $ActiveTrigger

var penguin_type = null

var game_paused = false


signal triggered(id, active)


func _ready():
	active_trigger.connect('pressed', self, '__on_active_pressed')
	
	EventManager.connect('game_paused', self, '_pause_game')


func set_data(id: String, active: bool, show_toggle: bool):
	penguin_type = id

	image.set_texture(DataHub.get_penguin_icon(id))
	name_label.set_text(DataHub.get_penguin_name(id))
	active_trigger.set_pressed(active)
	set_active_display(active)

	if show_toggle:
		active_trigger.show()
	else:
		active_trigger.hide()


func __on_active_pressed():
	if game_paused:
		active_trigger.set_pressed(!active_trigger.is_pressed())
		return
	
	var active = active_trigger.is_pressed()
	set_active_display(active)
	emit_signal('triggered', penguin_type, active)


func set_active_display(active: bool):
	if active:
		set_modulate(Color(1, 1, 1, 1))
	else:
		set_modulate(Color(1, 1, 1, 0.4))


func _pause_game(is_paused: bool):
	game_paused = is_paused
