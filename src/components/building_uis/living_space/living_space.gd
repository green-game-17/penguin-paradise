extends PanelContainer


signal produce_changed(id, active)


var building_object

var spawn_type_scene = load('res://components/building_uis/_entries/spawn_type_entry.tscn')

onready var slot1 = $VBoxContainer/CenterContainer/HBoxContainer/Penguins/MarginContainer/VBoxContainer/Slot1
onready var slot2 = $VBoxContainer/CenterContainer/HBoxContainer/Penguins/MarginContainer/VBoxContainer/Slot2
onready var type1 = $VBoxContainer/CenterContainer/HBoxContainer/Allowed/MarginContainer/VBoxContainer/Type1
onready var type2 = $VBoxContainer/CenterContainer/HBoxContainer/Allowed/MarginContainer/VBoxContainer/Type2
onready var progress_bar = $VBoxContainer/ProgressBar


func _ready():
	progress_bar.init_data('Getting hungry', 60)

	connect('visibility_changed', self, '__hidden')
	type1.connect('triggered', self, 'change_produces')
	type2.connect('triggered', self, 'change_produces')
	$VBoxContainer/Title.connect('closePressed', self, 'hide')


func set_building(new_building_object):
	building_object = new_building_object
	$VBoxContainer/Title.set_title(building_object.building_type)

	connect('produce_changed', building_object, 'change_produces')
	building_object.connect('set_progress', self, 'set_progress')
	building_object.connect('inhabitants_changed', self, '__refresh_dialogue')
	
	__refresh_dialogue()


func change_produces(id: String, active: bool):
	emit_signal('produce_changed', id, active)
	__play_sound()
	__refresh_dialogue()


func set_progress(new_value):
	progress_bar.set_value(new_value)


func __hidden():
	if not is_visible():
		disconnect('produce_changed', building_object, 'change_produces')
		building_object.disconnect('set_progress', self, 'set_progress')
		building_object.disconnect('inhabitants_changed', self, '__refresh_dialogue')
		__play_sound()


func __refresh_dialogue():
	__set_spawn_types()
	__set_slots()
	__set_progress_label()
	set_progress(building_object.hunger_value)


func __set_spawn_types():
	var types_living = DataHub.get_penguin_types_living(building_object.building_type)
	type1.set_data(types_living[0], types_living[0] in building_object.produces, types_living.size() > 1)
	if types_living.size() > 1:
		type2.set_data(types_living[1], types_living[1] in building_object.produces, true)
		type2.show()
	else:
		type2.hide()


func __set_slots():
	var inhabitants = building_object.inhabitants

	if inhabitants.size() > 0:
		slot1.set_to_penguin(inhabitants[0])
	else:
		slot1.set_to_empty()

	if inhabitants.size() > 1:
		slot2.set_to_penguin(inhabitants[1])
	else:
		slot2.set_to_empty()


func __set_progress_label():
	if building_object.inhabitants.size() > 0:
		progress_bar.set_label('Getting hungry')
	else:
		progress_bar.set_label('Searching for more penguins', false)


func __play_sound():
	$Click.play()
