extends HBoxContainer


onready var image = $Image
onready var name_label = $VBoxContainer/Name
onready var consumes_container = $VBoxContainer/Consumes
onready var consumes_name_label = $VBoxContainer/Consumes/Name
onready var consumes_image = $VBoxContainer/Consumes/Image
onready var consumes_amount_label = $VBoxContainer/Consumes/Amount

var placeholder_texture = load('res://assets/penguins/placeholder.png')

var penguin_type = null


func set_to_penguin(id):
	penguin_type = id

	name_label.set_text(DataHub.get_penguin_name(id))
	image.set_texture(DataHub.get_penguin_icon(id))

	var food_requirement = DataHub.get_food_requirements(id)

	consumes_container.show()
	consumes_name_label.set_text(DataHub.get_material_name(food_requirement.id))
	consumes_image.set_texture(DataHub.get_material_icon(food_requirement.id))
	consumes_amount_label.set_text(str(food_requirement.amount))


func set_to_empty():
	penguin_type = null

	name_label.set_text('Empty Space')
	image.set_texture(placeholder_texture)

	consumes_container.hide()
