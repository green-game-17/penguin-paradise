extends Building


onready var needs_column = $VBoxContainer/CenterContainer/HBoxContainer/Needs
onready var produces_column = $VBoxContainer/CenterContainer/HBoxContainer/Produces
onready var progress_bar = $VBoxContainer/ProgressBar


func _ready():
	$VBoxContainer/Title.set_title(ID.ICE_MACHINE)
	needs_column.init_materials(ID.ICE_MACHINE, true) # hide "per penguin"
	produces_column.init_materials(ID.ICE_MACHINE)
	progress_bar.init_data('Freezing', 20)


func set_building(new_building_object):
	.set_building(new_building_object)
	__refresh_dialogue()


func change_produces(id: String, active: bool):
	emit_signal('produce_changed', id, active)
	__refresh_dialogue()


func set_progress(value: float):
	progress_bar.set_value(value)
	

func __refresh_dialogue():
	produces_column.set_materials_state(building_object.produces, building_object.resources_paid_for)

	set_progress(building_object.collect_timer)
