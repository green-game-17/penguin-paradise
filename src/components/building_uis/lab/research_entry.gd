extends PanelContainer


signal select_research(data, stage)


var data
var unlocked = []
var stage = 1


func set_data(new_id, new_data):
	data = new_data
	data.id = new_id

	if data.infinite:
		$MarginContainer/HBoxContainer/VBoxContainer/Title.set_text(data.name + ' - Stage 1')
	else:
		$MarginContainer/HBoxContainer/VBoxContainer/Title.set_text(data.name)
	$MarginContainer/HBoxContainer/VBoxContainer/Description.set_text(data.description)
	$MarginContainer/HBoxContainer/Cost.set_text(str(data.cost * stage) + ' Points')

	if not 'requirements' in data:
		show()
	else:
		hide()

	_on_research_points_changed(StatisticsHub.get_research_points())

	StatisticsHub.connect('research_points_changed', self, '_on_research_points_changed')
	StatisticsHub.connect('unlock_building', self, 'check_unlock')
	$Button.connect('pressed', self, '_on_pressed')


func check_unlock(id, _count = 0):
	if 'requirements' in data and data.requirements.find(id) != -1 and unlocked.find(id) == -1:
		unlocked.append(id)
		if unlocked.size() == data.requirements.size():
			show()


func hide_research(id):
	if data.id == id:
		if data.infinite:
			stage += 1
			$MarginContainer/HBoxContainer/Cost.set_text(str(data.cost * stage) + ' Points')
			$MarginContainer/HBoxContainer/VBoxContainer/Title.set_text(data.name + ' - Stage ' + str(stage))
		else:
			hide()


func _on_research_points_changed(count: int):
	if count >= data.cost * stage:
		set_modulate(Color(1, 1, 1, 1))
		$Button.mouse_default_cursor_shape = CURSOR_POINTING_HAND
	else:
		set_modulate(Color(1, 1, 1, 0.5))
		$Button.mouse_default_cursor_shape = CURSOR_FORBIDDEN


func _on_pressed():
	emit_signal('select_research', data, stage)
