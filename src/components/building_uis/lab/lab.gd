extends Building


signal research_unlocked(id)
signal research_started(id)
signal hide_research(id)


const BAMBOO_VALUE = 4
const KWARDIUM_VALUE = 12


var current_research
var current_stage = 1
var research_active = false
var research_progress = 0
var researches_unlocked = []
var researched_stages = {}

var game_paused = false

onready var penguins_column = $VBoxContainer/CenterContainer/HBoxContainer/Penguins
onready var available_points_label = $VBoxContainer/CenterContainer/HBoxContainer/Points/MarginContainer/VBoxContainer/Available/Amount
onready var convert_container = $VBoxContainer/CenterContainer/HBoxContainer/Points/MarginContainer/VBoxContainer/Convert/MarginContainer/VBoxContainer
onready var progress_bar = $VBoxContainer/ProgressBar
onready var research_container = $VBoxContainer/ResearchContainer
onready var research_list = $VBoxContainer/ResearchContainer/MarginContainer/VBoxContainer


func _ready():
	$VBoxContainer/Title.set_title(ID.LAB)
	penguins_column.init_penguin_type(ID.LAB, true, 'Experimenting')
	penguins_column.connect('planned_changed', self, 'set_working_penguin')

	var convert_bamboo = convert_container.get_node('Bamboo')
	convert_bamboo.init_data(ID.BAMBOO, BAMBOO_VALUE)
	convert_bamboo.connect('convert_pressed', self, 'convert_to_rp')
	var convert_kwardium = convert_container.get_node('Kwardium')
	convert_kwardium.init_data(ID.KWARDIUM, KWARDIUM_VALUE)
	convert_kwardium.connect('convert_pressed', self, 'convert_to_rp')

	progress_bar.init_data('Researching', 100)
	progress_bar.hide()

	StatisticsHub.connect('research_points_changed', self, '_on_research_points_changed')

	EventManager.connect('game_paused', self, '_pause_game')
	EventManager.connect('get_save_data', self, 'get_save_data')
	EventManager.connect('set_save_data', self, 'set_save_data')
	
	for i in range(0, research_list.get_child_count()):
		research_list.get_child(i).queue_free() # remove preview entries
	var entry_scene = load('res://components/building_uis/lab/research_entry.tscn')
	for research_id in DataHub.RESEARCHES:
		var entry_node = entry_scene.instance()
		entry_node.set_data(research_id, DataHub.RESEARCHES[research_id])
		entry_node.connect('select_research', self, 'start_research')
		connect('hide_research', entry_node, 'hide_research')
		connect('research_unlocked', entry_node, 'check_unlock')
		research_list.add_child(entry_node)


func set_building(new_building_object):
	.set_building(new_building_object)
	__refresh_dialogue()


func _process(delta):
	if game_paused:
		return
	
	var working = StatisticsHub.get_penguin_stat('working', ID.FAIRY_PENGUIN)
	if working > 0 and research_active:
		research_progress += delta * pow(1.08, working)
		set_progress(research_progress)
		if research_progress >= current_research.duration * current_stage:
			research_progress = 0
			research_active = false
			if researches_unlocked.find(current_research.id) == -1:
				researches_unlocked.append(current_research.id)
			if current_research.infinite:
				researched_stages[current_research.id] = current_stage

			$Finished.play()
			
			set_progress(0)
			progress_bar.hide()
			research_container.show()
			_reset_height()

			emit_signal('research_unlocked', current_research.id)
			emit_signal('hide_research', current_research.id)
			if not current_research.infinite:
				StatisticsHub.set_building_unlocked(current_research.unlocks)
			else:
				for type in current_research.unlocks:
					match (type):
						'well_boost':
							StatisticsHub.update_well_boost_increase(current_research.unlocks[type])


func set_progress(value: float):
	progress_bar.set_value(value)


func _on_research_points_changed(count: int):
	var text = str(count)
	if count >= 1_000_000_000:
		text = 'way too much'
	elif count >= 1_000_000:
		text = '%s %03d %03d' % [
			floor(count / 1_000_000), floor((count % 1_000_000) / 1_000), count % 1_000
		]
	elif count >= 1_000:
		text = '%s %03d' % [floor(count / 1_000), count % 1_000]
	available_points_label.set_text(text)


func start_research(data, stage):
	if not research_active and StatisticsHub.get_research_points() >= data.cost * stage:
		current_research = data
		current_stage = stage
		
		research_container.hide()
		progress_bar.set_max(current_research.duration * current_stage)
		progress_bar.set_label('Researching %s'%current_research.name)
		progress_bar.show()
		_reset_height()

		StatisticsHub.update_research_points(-data.cost * current_stage)
		emit_signal("research_started", data.id)
		research_active = true


func __refresh_dialogue():
	penguins_column.set_planned(building_object.workers.amount)
	set_progress(research_progress)


func _reset_height():
	# We need to _really_ motivate Godot to re-size and re-position the panel while its visible
	rect_size.y = 0
	margin_top = 0
	margin_bottom = 0



func convert_to_rp(resource, amount):
	if StatisticsHub.get_material_stat(resource) >= amount:
		StatisticsHub.update_material_stat(resource, -amount)
		match resource:
			ID.BAMBOO:
				StatisticsHub.update_research_points(amount * BAMBOO_VALUE)
			ID.KWARDIUM:
				StatisticsHub.update_research_points(amount * KWARDIUM_VALUE)


func _pause_game(is_paused: bool):
	game_paused = is_paused


func set_save_data(key, data):
	if key == 'lab':
		current_research = data.current_research
		current_stage = data.get('current_stage', 1) # fallback, added later
		research_active = data.research_active
		research_progress = data.research_progress
		researches_unlocked = data.researches_unlocked
		researched_stages = data.get('researched_stages', {}) # fallback, added later
		for research in researches_unlocked:
			emit_signal('research_unlocked', research)
			if research in researched_stages:
				for stage in range(researched_stages[research]):
					emit_signal('hide_research', research)
			else:
				emit_signal('hide_research', research)


func get_save_data():
	var cur_res
	# duplicate on null doesn't work
	if current_research != null:
		cur_res = current_research.duplicate()
	EventManager.emit_signal('send_save_data', 'lab', {
		'current_research': cur_res,
		'current_stage': current_stage,
		'research_active': research_active,
		'research_progress': research_progress,
		'researches_unlocked': researches_unlocked.duplicate(),
		'researched_stages': researched_stages.duplicate(),
	})
