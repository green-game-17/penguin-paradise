extends HBoxContainer


var mat_id


signal convert_pressed(id, count)


func init_data(id: String, value: int):
	mat_id = id

	$Image.set_texture(DataHub.get_material_icon(id))
	$Name.set_text(DataHub.get_material_name(id))
	$ValueLabel.set_text('(%d Points each)'%value)

	var mat_count = StatisticsHub.get_material_stat(mat_id)
	_on_material_stat_changed(mat_id, mat_count)
	if mat_count == 0:
		hide()

	StatisticsHub.connect('material_stat_changed', self, '_on_material_stat_changed')

	$Button1.connect('pressed', self, '_on_pressed', [1])
	$Button10.connect('pressed', self, '_on_pressed', [10])
	$Button100.connect('pressed', self, '_on_pressed', [100])


func _on_material_stat_changed(id, count):
	if id == mat_id:
		if count >= 1:
			show()
			$Button1.show()
			$NoneLabel.hide()
		else:
			$Button1.hide()
			$NoneLabel.show()

		if count >= 10:
			$Button10.show()
		else:
			$Button10.hide()

		if count >= 100:
			$Button100.show()
		else:
			$Button100.hide()


func _on_pressed(count):
	emit_signal('convert_pressed', mat_id, count)
