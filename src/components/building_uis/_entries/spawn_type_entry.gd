extends HBoxContainer


signal produce_triggered(id, active)


var penguin_type

var game_paused = false


func _ready():
	EventManager.connect('game_paused', self, '_pause_game')


func set_data(type: String, active: bool):
	penguin_type = type
	$Icon.set_texture(DataHub.get_penguin_icon(penguin_type))
	$Name.set_text(DataHub.get_penguin_name(penguin_type))
	$SpawnsTrigger.set_pressed(active)


func set_toggle(active: bool):
	$SpawnsTrigger.set_pressed(active)


func _on_SpawnsTrigger_toggled(button_pressed: bool):
	if game_paused:
		$SpawnsTrigger.set_pressed(!button_pressed)
		return
	
	emit_signal('produce_triggered', penguin_type, button_pressed)


func _pause_game(is_paused: bool):
	game_paused = is_paused
