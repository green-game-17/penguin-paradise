extends HBoxContainer


signal closePressed


func _ready():
	$CloseButton.connect('pressed', self, '_on_close')


func set_title(id: String):
	$MarginContainer/Title.text = DataHub.get_building_name(id)


func set_title_text(text: String):
	$MarginContainer/Title.text = text


func _on_close():
	emit_signal('closePressed')
