extends PanelContainer


var data
var is_shown = true


func _ready():
	StatisticsHub.connect('unlock_building', self, 'show_building')
	EventManager.connect('hide_building_in_picker', self, 'set_building_built')
	
	connect('gui_input', self, '__on_entry_click')
	$ContentContainer/DetailsContainer/Costs/PanelContainer1.connect('gui_input', self, '__on_entry_click')
	$ContentContainer/DetailsContainer/Costs/PanelContainer2.connect('gui_input', self, '__on_entry_click')


func set_data(new_data):
	data = new_data

	if data.id == ID.MONUMENT_SITE:
		$ContentContainer/Image.set_texture(
			load('res://assets/buildings/monument/monument_preview.png')
		)
	else:
		$ContentContainer/Image.set_texture(
			load('res://assets/buildings/' + data.id + '/' + data.id + '_preview.png')
		)
		
	$ContentContainer/DetailsContainer/Name.set_text(data.name)

	var cost_panel_1 = $ContentContainer/DetailsContainer/Costs/PanelContainer1
	cost_panel_1.hint_tooltip = data.costs[0].id.capitalize()
	cost_panel_1.get_node('CostsEntry/Image').set_texture(
		load('res://assets/materials/%s.png'%data.costs[0].id)
	)
	cost_panel_1.get_node('CostsEntry/Amount').set_text(
		str(data.costs[0].amount)
	)

	var cost_panel_2 = $ContentContainer/DetailsContainer/Costs/PanelContainer2
	if data.costs.size() == 1:
		cost_panel_2.hide()
	else:
		cost_panel_2.hint_tooltip = data.costs[1].id.capitalize()
		cost_panel_2.get_node('CostsEntry/Image').set_texture(
			load('res://assets/materials/%s.png'%data.costs[1].id)
		)
		cost_panel_2.get_node('CostsEntry/Amount').set_text(
			str(data.costs[1].amount)
		)

	if data.id != ID.TENT and data.id != ID.GATHERING_HUB and data.id != ID.LAB:
		hide_building(data.id)


func show_building(id):
	if id == data.id:
		is_shown = true
		set_modulate(Color(1, 1, 1, 1))
		mouse_default_cursor_shape = CURSOR_POINTING_HAND
		$ContentContainer/DetailsContainer/Costs/PanelContainer1.mouse_default_cursor_shape = CURSOR_POINTING_HAND
		$ContentContainer/DetailsContainer/Costs/PanelContainer2.mouse_default_cursor_shape = CURSOR_POINTING_HAND
		hint_tooltip = ''


func hide_building(id):
	if id == data.id:
		is_shown = false
		set_modulate(Color(1, 1, 1, 0.3))
		mouse_default_cursor_shape = CURSOR_FORBIDDEN
		$ContentContainer/DetailsContainer/Costs/PanelContainer1.mouse_default_cursor_shape = CURSOR_FORBIDDEN
		$ContentContainer/DetailsContainer/Costs/PanelContainer2.mouse_default_cursor_shape = CURSOR_FORBIDDEN
		hint_tooltip = 'Not researched yet'


func set_building_built(id):
	if id == data.id:
		is_shown = false
		$Checkmark.show()
		set_modulate(Color(1, 1, 1, 0.8))
		$ContentContainer.set_modulate(Color(1, 1, 1, 0.2))
		mouse_default_cursor_shape = CURSOR_FORBIDDEN
		$ContentContainer/DetailsContainer/Costs/PanelContainer1.mouse_default_cursor_shape = CURSOR_FORBIDDEN
		$ContentContainer/DetailsContainer/Costs/PanelContainer2.mouse_default_cursor_shape = CURSOR_FORBIDDEN
		hint_tooltip = "Already built"
	
	
func __on_entry_click(event):
	if event is InputEventMouseButton and event.get_button_index() == BUTTON_LEFT and is_shown:
		var sound = get_node_or_null('Sound')
		if sound:
			sound.play()
		EventManager.emit_signal("building_select", data)
