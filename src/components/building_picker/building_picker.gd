extends Control


var template_node = preload("res://components/building_picker/building_picker_entry.tscn")


func _ready():
	$VBoxContainer/Title.set_title_text('Buildings')
	$VBoxContainer/Title.connect('closePressed', self, 'hide')

	var grid_node = $VBoxContainer/CenterContainer/GridContainer
	for entry_id in DataHub.BUILDINGS.keys():
		var entry_value = DataHub.BUILDINGS[entry_id]
		if not 'build_costs' in entry_value:
			continue
		var entry_data = { id=entry_id, name=entry_value.name, costs=entry_value.build_costs}
		var entry_node = template_node.instance()
		entry_node.set_data(entry_data)
		grid_node.add_child(entry_node)
	grid_node.set_columns(4)
