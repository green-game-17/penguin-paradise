extends StaticBody


signal set_progress(value)
signal set_monument_progress()
signal removed_penguin(id)
signal inhabitants_changed()
signal new_production() # fired at the start of a new progress


export var building_type: String
export var starter = false

const HUNGER_MAX = 60
const MONUMENT_GOAL = 500
const BASE_SPAWN_TIME = 20
const RANDOM_SPAWN_TIME = 40

var resources_paid_for = 0
var produces = []
var inhabitants = []
var workers = {
	'type': '',
	'amount': 0
}
var collect_timer = 0
var timer_limit = 20
var hunger_value = 0
var monument_progress = 0

var game_paused = false


func _ready():
	randomize()
	EventManager.connect('game_paused', self, '_pause_game')
	
	if starter:
		StatisticsHub.update_building_built(ID.TENT)
		inhabitants = [ID.CAPE_PENGUIN, ID.CAPE_PENGUIN]
		set_data(building_type)


func set_data(type):
	building_type = type
	if DataHub.has_building_gen_mats(building_type):
		var generated_materials = DataHub.get_generated_materials(building_type)
		for material in generated_materials:
			produces.append(material.id)
	if building_type == ID.MONUMENT_SITE:
		$CollisionShape.set_scale(Vector3(5, 0.75, 5))
	if building_type in [ID.TENT, ID.HOUSE, ID.IGLOO, ID.CHARGING_STATION]:
		for type in DataHub.get_penguin_types_living(building_type):
			produces.append(type)
		if inhabitants.size () < 2:
			$SpawnTimer.set_wait_time(BASE_SPAWN_TIME + randi() % RANDOM_SPAWN_TIME)
			if $SpawnTimer.is_inside_tree():
				$SpawnTimer.start()
			else:
				$SpawnTimer.set_autostart(true)


func set_selected(is_selected):
	var child = get_child(2)
	if child is Spatial:
		if is_selected:
			child.set_scale(child.get_scale() * 0.5)
		else:
			child.set_scale(child.get_scale() * 2)


func set_working_count(type, new_count):
	emit_signal('set_progress', collect_timer)
	if building_type == ID.MINING_HUB:
		StatisticsHub.update_well_assigned_penguins(new_count - workers.amount)
	workers.type = type
	workers.amount = new_count


func set_timer_limit(limit: int):
	timer_limit = limit


func change_produces(id: String, active: bool):
	if active and not id in produces:
		produces.append(id)
		collect_timer = 0
	elif not active and id in produces and produces.size() > 1:
		produces.remove(produces.find(id))
		collect_timer = 0
		if building_type in [ID.TENT, ID.HOUSE, ID.IGLOO, ID.CHARGING_STATION]:
			var removed = false
			if inhabitants.size() == 2 and not inhabitants[1] in produces:
				var penguin_type = inhabitants[1]
				var living_penguins = StatisticsHub.update_penguin_stat('living', inhabitants[1], -1)
				inhabitants.remove(1)
				removed = true
				if StatisticsHub.get_penguin_stat('working', penguin_type) > living_penguins:
					emit_signal('removed_penguin', penguin_type)
			if inhabitants.size() > 0 and not inhabitants[0] in produces:
				var penguin_type = inhabitants[0]
				var living_penguins = StatisticsHub.update_penguin_stat('living', inhabitants[0], -1)
				inhabitants.remove(0)
				removed = true
				if StatisticsHub.get_penguin_stat('working', penguin_type) > living_penguins:
					emit_signal('removed_penguin', penguin_type)
			if removed:
				if inhabitants.size() == 0:
					hunger_value = 0
				if $SpawnTimer.is_stopped():
					$SpawnTimer.set_wait_time(BASE_SPAWN_TIME + randi() % RANDOM_SPAWN_TIME)
					$SpawnTimer.start()
				emit_signal('inhabitants_changed')
	emit_signal('set_progress', collect_timer)


func open_dialogue():
	EventManager.emit_signal("open_dialogue", self)


func remove_self():
	if not building_type in [ID.MONUMENT, ID.MONUMENT_SITE, ID.POND, ID.WELL]:
		if inhabitants.size() > 0:
			for penguin_type in inhabitants:
				var living_penguins = StatisticsHub.update_penguin_stat('living', penguin_type, -1)
				if StatisticsHub.get_penguin_stat('working', penguin_type) > living_penguins:
					emit_signal('removed_penguin', penguin_type)
		if workers.amount > 0:
			StatisticsHub.update_penguin_stat('working', workers.type, -workers.amount)

		for cost in DataHub.get_building_costs(building_type):
			StatisticsHub.update_material_stat(cost.id, cost.amount)

		StatisticsHub.update_building_demolished(building_type)

		queue_free()


func _on_SpawnTimer_timeout():
	var penguin_types = DataHub.get_penguin_types_living(building_type)
	if produces.size() == 0:
		prints(building_type, produces, get_translation())
	var type = produces[randi() % produces.size()]
	inhabitants.append(type)
	StatisticsHub.update_penguin_stat('living', type, 1)
	hunger_value = 0
	emit_signal('inhabitants_changed')
	
	if inhabitants.size() < 2:
		$SpawnTimer.set_wait_time(BASE_SPAWN_TIME + randi() % RANDOM_SPAWN_TIME)
		$SpawnTimer.start()


func _process(delta):
	if game_paused:
		return
	
	if inhabitants.size() > 0:
		hunger_value += delta
		emit_signal('set_progress', min(hunger_value, HUNGER_MAX))
		if hunger_value >= HUNGER_MAX:
			hunger_value = 0
			if inhabitants.size() == 1:
				__check_food_requirements(inhabitants[0], 0)
			else:
				var food_requirements_one = DataHub.get_food_requirements(inhabitants[0])
				var food_requirements_two = DataHub.get_food_requirements(inhabitants[1])
				if food_requirements_one.amount <= food_requirements_two.amount:
					__check_food_requirements(inhabitants[1], 1)
					__check_food_requirements(inhabitants[0], 0)
				else:
					if StatisticsHub.get_material_stat(food_requirements_one.id) < food_requirements_one.amount:
						var living_penguins = StatisticsHub.update_penguin_stat('living', inhabitants[0], -1)
						var penguin_type = inhabitants[0]
						inhabitants.remove(0)
						emit_signal('inhabitants_changed')
						if StatisticsHub.get_penguin_stat('working', penguin_type) > living_penguins:
							emit_signal('removed_penguin', penguin_type)
						if $SpawnTimer.is_stopped():
							$SpawnTimer.set_wait_time(BASE_SPAWN_TIME + randi() % RANDOM_SPAWN_TIME)
							$SpawnTimer.start()
						__check_food_requirements(inhabitants[0], 0)
					else:
						StatisticsHub.update_material_stat(food_requirements_one.id, -food_requirements_one.amount)
						__check_food_requirements(inhabitants[1], 1)
	if building_type == ID.MONUMENT_SITE:
		if resources_paid_for == 0:
			var required_materials = DataHub.get_required_materials(building_type)
			var worker_amount = StatisticsHub.get_penguin_stat('living', ID.ROBOT_PENGUIN)
			var max_workers_possible = min(worker_amount, MONUMENT_GOAL - monument_progress)
			for resource in required_materials:
				var max_ressources = StatisticsHub.get_material_stat(resource.id) / resource.amount
				if max_ressources < max_workers_possible:
					max_workers_possible = max_ressources
			if max_workers_possible > 0:
				for resource in required_materials:
					StatisticsHub.update_material_stat(resource.id, -resource.amount * max_workers_possible)
				resources_paid_for = max_workers_possible
				emit_signal('new_production')
				reduce_monument_timer(delta)
		else:
			reduce_monument_timer(delta)
	elif ((resources_paid_for > 0 or workers.amount > 0) and not building_type in [ID.LAB, ID.MINING_HUB]) or building_type == ID.ICE_MACHINE:
		if DataHub.has_building_req_mats(building_type) and resources_paid_for == 0:
			var required_materials = DataHub.get_required_materials(building_type)
			var worker_amount = 1
			if building_type != ID.ICE_MACHINE:
				worker_amount = workers.amount
			var has_resources = true
			for resource in required_materials:
				if StatisticsHub.get_material_stat(resource.id) < resource.amount * worker_amount:
					has_resources = false
					break
			if has_resources:
				for resource in required_materials:
					StatisticsHub.update_material_stat(resource.id, -resource.amount * worker_amount)
				resources_paid_for = worker_amount
				emit_signal('new_production')
				reduce_timer(delta)
		else:
			if collect_timer == 0:
				resources_paid_for = workers.amount
				emit_signal('new_production')
			reduce_timer(delta)


func __check_food_requirements(id, index):
	var food_requirements = DataHub.get_food_requirements(id)
	if StatisticsHub.get_material_stat(food_requirements.id) < food_requirements.amount:
		var living_penguins = StatisticsHub.update_penguin_stat('living', id, -1)
		inhabitants.remove(index)
		emit_signal('inhabitants_changed')
		if StatisticsHub.get_penguin_stat('working', id) > living_penguins:
			emit_signal('removed_penguin', id)
		if $SpawnTimer.is_stopped():
			$SpawnTimer.set_wait_time(BASE_SPAWN_TIME + randi() % RANDOM_SPAWN_TIME)
			$SpawnTimer.start()
	else:
		StatisticsHub.update_material_stat(food_requirements.id, -food_requirements.amount)


func reduce_timer(delta):
	if resources_paid_for == 0:
		return
	
	collect_timer += delta
	emit_signal('set_progress', collect_timer)
	if collect_timer >= timer_limit:
		collect_timer = 0
		var generated_materials = DataHub.get_generated_materials(building_type)
		if produces.size() == 0:
			prints(building_type, produces, get_translation())
		var material = produces[randi() % produces.size()]
		var index = 0
		for i in range(generated_materials.size()):
			if generated_materials[i].id == material:
				index = i
				break
		StatisticsHub.update_material_stat(material, resources_paid_for * generated_materials[index].amount)
		resources_paid_for = 0
		emit_signal('new_production')
		emit_signal('set_progress', 0)


func reduce_monument_timer(delta):
	collect_timer += delta
	emit_signal('set_progress', collect_timer)
	if collect_timer >= timer_limit:
		collect_timer = 0
		monument_progress += resources_paid_for
		resources_paid_for = 0
		emit_signal('new_production')
		emit_signal("set_monument_progress", monument_progress)
		if monument_progress >= MONUMENT_GOAL:
			var rotations = Vector3.ZERO
			for child in get_children():
				if child is Spatial:
					rotations = child.get_rotation()
					child.queue_free()
			var monument_scene = load('res://components/buildings/monument.tscn')
			var monument_node = monument_scene.instance()
			monument_node.set_rotation(rotations)
			add_child(monument_node)
			building_type = ID.MONUMENT
			StatisticsHub.update_building_built(ID.MONUMENT)


func _pause_game(is_paused: bool):
	if not $SpawnTimer.is_stopped():
		$SpawnTimer.set_paused(is_paused)
	game_paused = is_paused


func set_save_data(data):
	building_type = data.type
	set_translation(Vector3(data.translation.x, data.translation.y, data.translation.z))
	set_rotation(Vector3(data.rotation.x, data.rotation.y, data.rotation.z))
	if 'inhabitants' in data:
		inhabitants = data.inhabitants
		if inhabitants.size() < 2 and $SpawnTimer.is_stopped():
			$SpawnTimer.set_wait_time(BASE_SPAWN_TIME + randi() % RANDOM_SPAWN_TIME)
			$SpawnTimer.start()
	if 'produces' in data:
		produces = data.produces
	if 'hunger' in data:
		hunger_value = data.hunger
	if 'workers' in data:
		workers = data.workers
	if 'collect' in data:
		collect_timer = data.collect
	if 'resources_paid_for' in data:
		resources_paid_for = data.resources_paid_for
	if 'progress' in data:
		monument_progress = data.progress


func get_save_data() -> Dictionary:
	match building_type:
		ID.TENT, ID.HOUSE, ID.IGLOO, ID.CHARGING_STATION:
			return {
				'type': building_type,
				'translation': {
					'x': get_translation().x,
					'y': get_translation().y,
					'z': get_translation().z,
				},
				'rotation': {
					'x': get_rotation().x,
					'y': get_rotation().y,
					'z': get_rotation().z,
				},
				'inhabitants': inhabitants.duplicate(),
				'produces': produces.duplicate(),
				'hunger': hunger_value,
			}
		ID.GATHERING_HUB, ID.BLACKSMITH, ID.FACTORY:
			return {
				'type': building_type,
				'translation': {
					'x': get_translation().x,
					'y': get_translation().y,
					'z': get_translation().z,
				},
				'rotation': {
					'x': get_rotation().x,
					'y': get_rotation().y,
					'z': get_rotation().z,
				},
				'workers': workers.duplicate(),
				'produces': produces.duplicate(),
				'collect': collect_timer,
				'resources_paid_for': resources_paid_for,
			}
		ID.MINING_HUB, ID.LAB:
			return {
				'type': building_type,
				'translation': {
					'x': get_translation().x,
					'y': get_translation().y,
					'z': get_translation().z,
				},
				'rotation': {
					'x': get_rotation().x,
					'y': get_rotation().y,
					'z': get_rotation().z,
				},
				'workers': workers.duplicate(),
			}
		ID.ICE_MACHINE:
			return {
				'type': building_type,
				'translation': {
					'x': get_translation().x,
					'y': get_translation().y,
					'z': get_translation().z,
				},
				'rotation': {
					'x': get_rotation().x,
					'y': get_rotation().y,
					'z': get_rotation().z,
				},
				'collect': collect_timer,
				'produces': produces.duplicate(),
			}
		ID.MONUMENT_SITE:
			return {
				'type': building_type,
				'translation': {
					'x': get_translation().x,
					'y': get_translation().y,
					'z': get_translation().z,
				},
				'rotation': {
					'x': get_rotation().x,
					'y': get_rotation().y,
					'z': get_rotation().z,
				},
				'workers': workers.duplicate(),
				'collect': collect_timer,
				'resources_paid_for': resources_paid_for,
				'progress': monument_progress,
			}
		_:
			return {
				'type': building_type,
				'translation': {
					'x': get_translation().x,
					'y': get_translation().y,
					'z': get_translation().z,
				},
				'rotation': {
					'x': get_rotation().x,
					'y': get_rotation().y,
					'z': get_rotation().z,
				},
			}
